import React, { Component } from 'react';
import Modal from 'react-awesome-modal';

import axios from 'axios';

var config = require('./../../../config.js').config;
var host = config.host;

export default class Popup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            status:'SAVE',
            readonly:true,
            product:this.props.product,
            visible : false,
            variant:this.props.product.variant
            
        }
        this.handleChange = this.handleChange.bind(this);
    }

    openModal() {
        
        
        this.setState({
          visible : true
      });
      this.handleChange = this.handleChange.bind(this)
    }

    closeModal() {
        this.setState({
            aliexpressId:'',
            visible : false
        });
    }

    handleClick(e) {
      console.log(this.state.variant);
      e.preventDefault();
      var jsons = new Array();
      jsons.push({
        shopifyProductId:this.state.product.shopify_product_id,
        });
      jsons.push(this.state.variant);

      axios.post(
  window.encodeURI(
          host+':3000/api/update_variant'
        ),jsons
      )
      .then(response => {});

    }

    handleChange(event) {
      
        var Table_Variant=[];
        var arr=[[]];
        Object.values(JSON.parse(this.state.variant)).forEach(element => {
          element.forEach(element1 => {
  
            Table_Variant.push(element1);
          }); 
        });
        
        console.log(event.target.className);
        console.log(Table_Variant[parseInt(event.target.className)]);
        console.log(Table_Variant[parseInt(event.target.className)].aliexpress);
        Table_Variant[parseInt(event.target.className)].aliexpress=event.target.value;
        arr[0].push(Table_Variant);
      this.setState({variant: JSON.stringify(arr[0])});
    }



    renderList() {
      console.log(this.state.variant);
      var variant=Object.values(JSON.parse(this.state.variant));
      console.log(variant);
      var Table_Variant=[];
      variant=variant.map(x=>x);
      variant.forEach(element => {
        element.forEach(element1 => {

          Table_Variant.push(element1);
        }); 
      });
      

      console.log(variant);
        return (
            <section>
                <input type="button" value="Variants" onClick={() => this.openModal()} />
                <Modal visible={this.state.visible}  effect="fadeInUp" onClickAway={() => this.closeModal()}>
                    <div class='modal-content'>
                        <h1 style={{marginLeft: '10px'}}>{this.state.product.name}</h1>
                        <h2 style={{marginLeft: '10px'}}>Variant Edition</h2>
                        <table className="table table-striped">
               <thead>
                  <tr>
            <th>Shopify</th>
            <th>Aliexpress</th>
          </tr>
              </thead>
                  <tbody>
                        {Table_Variant.map((variant, index) => 
                        
                        <tr><td><input readOnly='true' class={index}type="text" value={Object.values(variant)[0]} key={index} onChange={this.handleChange} /></td>
                            <td><input  class={index} type="text" value={Object.values(variant)[1]} key={index} onChange={this.handleChange} /></td>
                              <td><button style={{marginLeft: '10px'}}  type="button" class="btn btn-primary btn-sm" onClick={this.handleClick.bind(this)}>
                            {this.state.status}
                              </button>     </td>
                        </tr>
                        )
                           }
                        
                    </tbody>
                    
      </table>  
      <a style={{marginLeft: '10px'}}   href="javascript:void(0);" onClick={() => this.closeModal()}>Close</a>
      </div>
                </Modal>
            </section>
        );
    }

    render() {
      return this.renderList();
    }
}