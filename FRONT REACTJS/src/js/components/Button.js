import React from 'react';
import axios from 'axios';

var config = require('./../../../config.js').config;
var host = config.host;

import Popup from './Popup';
export default class Button extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      index:this.props.index,
      product:this.props.product,
      readonly:true,
      status: "EDIT",
      sell_price:this.props.product.sell_price,
      buy_price:this.props.product.buy_price,
      stock:this.props.product.stock.replace(/\s/g, ''),
      aliexpressId: this.props.product.aliexpress_product_id,
      ShopId:this.props.product.shopify_product_id,
      products:[],    
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleChange1 = this.handleChange1.bind(this);
    this.handleChange2 = this.handleChange2.bind(this);
    this.handleChange3 = this.handleChange3.bind(this);
  }
  handleChange(event) {
    this.setState({aliexpressId: event.target.value});
  }
  handleChange1(event) {
    this.setState({sell_price: event.target.value});
  }
  handleChange2(event) {
    this.setState({buy_price: event.target.value});
  }
  handleChange3(event) {
    this.setState({stock: event.target.value});
  }
  handleClick(e) {
    e.preventDefault();
    if(this.state.status=="SAVE"){
    this.setState({ status: "EDIT",readonly: true });
    axios.get(
window.encodeURI(
        host + `:3000/api/update_product?shopifyProductId=`+this.state.product.shopify_product_id+'&aliexpressId='+this.state.aliexpressId+'&sell_price='+this.state.sell_price+'&buy_price='+this.state.buy_price+'&stock='+this.state.stock,
      ),
    )
    .then(response => {});
   
  }else{this.setState({ status: "SAVE",readonly: false }) }}
render(){
    return (
      <tr><td>{this.state.index}</td>
      <td className="repo-name">{this.state.product.shop_id}</td>
      <td className="repo-name">{this.state.product.shopify_product_id}</td>
      <td className="repo-name">{this.state.product.name}</td>
      <td><input readOnly={this.state.readonly} type="text" value={this.state.aliexpressId} onChange={this.handleChange} /></td>
      <td><input readOnly={this.state.readonly} type="text" value={this.state.sell_price} onChange={this.handleChange1} /></td>
      <td><input readOnly={this.state.readonly} type="text" value={this.state.buy_price} onChange={this.handleChange2} /></td>
      <td><input readOnly={this.state.readonly} type="text" value={this.state.stock == null ? '' : this.state.stock} onChange={this.handleChange3} /></td>
      <td><Popup product={this.state.product} /></td>
      
      <td>
    <div>
     <button style={{marginLeft: '10px'}}  type="button" class="btn btn-primary btn-sm" onClick={this.handleClick.bind(this)}>
    {this.state.status}{this.state.value}
      </button>
      {this.state.products}
      </div>
      </td>
      </tr>
    );
  }


}
