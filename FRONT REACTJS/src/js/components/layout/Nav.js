import React from "react";
import { IndexLink, Link } from "react-router";
import Cookies from 'universal-cookie';

    /*const logComponent =()=>{
      // fonctionne pas je sais pas pourquoi, il ne s'affiche pas :(
       render(){
        return <li class={loginClass}>
                <IndexLink to="login" onClick={this.toggleCollapse.bind(this)}>Log in</IndexLink>
                </li>
      }     
    }
     const profilComponent =({param})=>{
        return <li class={profilClass}>
                <Link to="profil" onClick={this.toggleCollapse.bind(this)}>{param}</Link>
                </li>
      }*/
export default class Nav extends React.Component {
  constructor() {
    super()
    this.state = {
      collapsed: true,
    };
  }

  toggleCollapse() {
    const collapsed = !this.state.collapsed;
    this.setState({collapsed});
  }


  
  render() {
    var cookies = new Cookies(cookies);
    var id = cookies.get('login');
    const { location } = this.props;
    const { collapsed } = this.state;
    const loginClass = location.pathname === "/" ? "active" : "";
    const archivesClass = location.pathname.match(/^\/archives/) ? "active" : "";
    const settingsClass = location.pathname.match(/^\/settings/) ? "active" : "";
    const profilClass = location.pathname.match(/^\/profil/) ? "active" : "";
    const featuredClass = location.pathname.match(/^\/campagne/) ? "active" : "";
    const analyticsClass = location.pathname.match(/^\/analytics/) ? "active" : "";
    const signClass= location.pathname.match(/^\/sign/) ? "active" : "";
    const adminClass= location.pathname.match(/^\/admin/) ? "active" : "";

    const navClass = collapsed ? "collapse" : "";

      const containerStyle = {
      float:'right',

      };

    return (
      <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">

        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" onClick={this.toggleCollapse.bind(this)} >
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>

          </div>

          <div class={"navbar-collapse " + navClass} id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav" >
              <li class={featuredClass}>
                <Link to="/campagne" onClick={this.toggleCollapse.bind(this)}>Marketing</Link>
              </li>
              <li class={analyticsClass}>
                <Link to="analytics" onClick={this.toggleCollapse.bind(this)}>Suivi</Link>
              </li>
              <li class={archivesClass}>
                <Link to="archives" onClick={this.toggleCollapse.bind(this)}>Orders</Link>
              </li>

              <li class={settingsClass}>
                <Link to="settings" onClick={this.toggleCollapse.bind(this)}>Products</Link>
              </li>
              <li class={settingsClass}>
                <Link to="/admin" onClick={this.toggleCollapse.bind(this)}>Admin</Link>
              </li>
			  <li class={settingsClass}>
                <Link to="instalytics" onClick={this.toggleCollapse.bind(this)}>instalytics</Link>
              </li>

            </ul>

            <ul class="nav navbar-nav" style={containerStyle}>

              {id?
                <li class={profilClass}>
                <Link to="profil" onClick={this.toggleCollapse.bind(this)}>{id}</Link>
                </li>
                :
                <li class={loginClass}>
                <IndexLink to="/" onClick={this.toggleCollapse.bind(this)}>Log in</IndexLink>
                </li>
              }

            </ul>
          </div>
        </div>

      </nav>
    );
  }

}

/// du code
/*

*/

