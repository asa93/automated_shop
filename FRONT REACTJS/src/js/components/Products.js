import React from 'react';
import axios from 'axios';
import Button from './Button';
import Cookies from 'universal-cookie';

var config = require('./../../../config.js').config;
var host = config.host;

const Product = ({ product, index }) =>
  <tr>
    <td>{index + 1}</td>
    <td className="repo-name">{product.shopify_product_id}</td>
    <td>{product.sell_price} Euros</td>
    <td><Button ProductIdAliex={product}/></td>
  </tr>;

export default class Products extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      products: [],
      loading: true,
      error: null,
      value: '',
    };
   
  }
 
  componentDidMount() {
    var cookies = new Cookies(cookies);
    var cookie_login = cookies.get('login');
    axios
      .get(
        window.encodeURI(
          host + ':3000/api/products/'+cookie_login+'',
        ),
      )
      .then(response => {

        const products = response.data;
		   
        this.setState({
          products,
          loading: false,
        });
      })
      .catch(error => {
        this.setState({
          error: error,
          loading: false,
        });
      });
  }
  
  renderLoading() {
    return (
      <div>
        Loading...
      </div>
    );
  }

  handleClick(e) {
    e.preventDefault();
    axios
      .get(
        window.encodeURI(
          'http://localhost:3000/api/insertNewProducts',
        ),
      ).then(response => {
      window.location.reload();
      });
  }


  renderList() {
    const products = this.state.products;
    const error=this.state.error;
    console.log('totot');
    console.log(products);


    return (
	<div>
    <h1>{this.props.title}</h1>
    <button type="button" class="btn btn-primary" onClick={this.handleClick.bind(this)}>Refresh Products</button>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>#</th>
            <th>Shop</th>
            <th>Product ID</th>
            <th>Name</th>
            <th>Aliexpress ID</th>
            <th>Sell Price</th>
            <th>Buy Price</th>
            <th>Stock</th>
          </tr>
        </thead>
        <tbody>
          {products.map((product, index) =>
            <Button product={product} index={index} key={product.Id} />,
          )}
		  
        </tbody>
      </table>
	  <p>{JSON.stringify(this.state)}</p></div>
    );
  }

  render() {
    return this.state.loading ? this.renderLoading() : this.renderList();
  }
}
