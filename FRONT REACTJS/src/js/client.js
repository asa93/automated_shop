import React from "react";
import ReactDOM from "react-dom";
import { Router, Route, IndexRoute, hashHistory } from "react-router";


import Admin from "./pages/Admin";
import Archives from "./pages/Archives";
import Featured from "./pages/Featured";
import Layout from "./pages/Layout";
import Settings from "./pages/Settings";
import Analytics from "./pages/Analytics";
import Login from "./pages/Login";
import Profil from "./pages/Profil";
import App from './scripts/components/app-main';
import Sign from "./pages/Sign";


const app = document.getElementById('app');

 

ReactDOM.render(
  <Router history={hashHistory}>
    <Route path="/" component={Layout}>
      <Route path="/campagne"component={Featured}></Route>
      <Route path="archives(/:article)" name="archives" component={Archives}></Route>
       <Route path="analytics" name="analytics" component={Analytics}></Route>
	   <Route path="/instalytics"component={App}></Route>
       <Route path="/admin"component={Admin}></Route>
      <Route path="settings" name="settings" component={Settings}></Route>
      <IndexRoute component={Login}></IndexRoute>
      <Route path="/profil" name='profil' component={Profil}></Route>
      <Route path='/sign' name='sign' component={Sign}></Route>
    </Route>
  </Router>,
app);
