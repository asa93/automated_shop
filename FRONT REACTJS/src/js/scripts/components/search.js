import React, { Component } from "react";
import ReactDOM from "react-dom";

class SearchBox extends Component {
  handleChange(event) {
    event.target.select();
  }
  render() {
    return (
      <div className="col-xs-12 search-container nopadding">
        <div className="row">
          <div className="col-xs-0 col-sm-4 col-lg-4">
           
          </div>
          <div className="col-xs-12 col-sm-4 col-lg-4 ">
            <form className="searchbox text-left">
              {/* <label> */}
              <input
                ref="search suggestion"
                onClick={this.handleChange}
                className="searchbox__input typeahead form-control"
                type="search"
                placeholder="Search Instagram Username..."
                id="q"
              />
              {/* </label> */}
            </form>
          </div>
          <div className="col-xs-0 col-sm-4 col-lg-4">
		  </div>
        </div>
      </div>
    );
  }
}
module.exports = SearchBox;
