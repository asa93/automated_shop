import React, { Component } from 'react';
import ReactDOM from 'react-dom';
let backdropIMG;

class Card extends Component {

  render() {
    let data = this.props.data
      // if movie ID found, then...



      let posterIMG = data.poster,
          production = data.production,
          productionCountries = data.production_countries,
          genres = data.genre,
          totalRevenue = data.revenue,
          productionList = 'PIERROT'/*nestedDataToString(production)*/,
          productionCountriesList ='PIERROT'/* nestedDataToString(productionCountries)*/,
          noData = '-',
          genresList ='PIERROT' /*nestedDataToString(genres)*/;
          backdropIMG =  data.backdrop;



      // conditional statements for no data
       if (data.vote === 'undefined' || data.vote === 0) {
          data.vote = noData
        } else {
          data.vote = data.vote 
        };

      if (totalRevenue === 'undefined' || totalRevenue === 0) {
           totalRevenue = noData
         } else {
           totalRevenue = data.revenue/*numeral(data.revenue).format('(0,0)')*/;
         };

      if(data.poster== null){
        posterIMG = 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSols5HZxlQWyS9JY5d3_L9imbk0LiziHiyDtMZLHt_UNzoYUXs2g';
      }



      return (
	    
        <div className="box">
    <div className="container">
	
		<div className="poster-container nopadding column six ">
		
            
            <img id="postertest" className='poster round' src={posterIMG}/>
         </div>
		 
          <div className="meta-data-container column three">
	
		<h1>{data.original_title}</h1>	
            <span className="tagline">{data.tagline}</span>
            <p>{data.overview}</p>
				<div className="additional-details text">
              <span className="genre-list">Loyalty :</span>
              <span className="production-list">{Math.round((totalRevenue/data.followers)*100*100)/100} %</span>
              <div className="row nopadding release-details">
                <div className="col-xs-6"> Followers: <span className="meta-data">{data.followers}</span></div>
                <div className="col-xs-6"> Following: <span className="meta-data">{data.runtime}</span> </div>
                <div className="col-xs-6"> Average likes on posts: <span className="meta-data">{productionCountries}</span></div>
                <div className="col-xs-6"> Number of posts <span className="meta-data">{data.production}</span></div>
              </div>
            </div>
          </div>
        </div>
		</div>

      )
    }
 /* componentDidUpdate() {
    document.body.style.backgroundColor = 'white';
  }*/
}


function nestedDataToString(nestedData) {
  let nestedArray = [],
      resultString;
  if(nestedData !== undefined){
    nestedData.forEach(function(item){
      nestedArray.push(item.name);
    });
  }
  resultString = nestedArray.join(', '); // array to string
  return resultString;
};
module.exports = Card;
