import React, { Component } from 'react';
import SearchBox from './search';
import Card from './card';
import Post from './post';
import axios from 'axios';

var config = require('./../../../config.js').config;
var host = config.host;


class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      movieID: 'ARTZCENTRAL' // set initital load movie - Interstellar
    }
  }
  render() {
	 let mostliked=this.state.mostLiked;
	 let mostComment=this.state.mostComment;
	 let listposts='';
	 let listcommentposts='';
	 if (typeof mostliked=== 'object') {
           listposts = this.MyList(mostliked)/*numeral(data.revenue).format('(0,0)')*/;
         };
		 if (typeof mostComment=== 'object') {
           listcommentposts = this.MyList(mostComment)/*numeral(data.revenue).format('(0,0)')*/;
         };
    return (
	
	<div id="outer-container">
	
        
    <div class="container">
	<SearchBox fetchMovieID={this.fetchMovieID.bind(this)}/>
		<section>
		<div className='card'  style={{marginTop: '10px'}}>
		
        <Card data={this.state}/>
		 </div>
		 </section>
		<div className="column twelve cardcont nopadding" >
		 <span className="text-center" style={{flex:1, flexDirection: 'row',alignItems: 'center',color: 'white'}}><h1 style={{borderTop: '3px solid white',borderBottom: 'px solid white'}}>Image Downloader</h1></span>
		 </div>
		 <div>
		 <section>
		 <div className="card">
		 <div className="card columns one-half" style={{borderRight: '3px solid white'}}>
		 <span className="tagline text-center" style={{flex:1, flexDirection: 'row',alignItems: 'center'}}>Most Liked Posts</span>
		{listposts}
		</div>
		<div className="card columns one-half" style={{borderLeft: '3px solid white'}}>
		<span className="tagline text-center" style={{flex:1, flexDirection: 'row',alignItems: 'center'}}>Most Commented Posts</span>
		{listcommentposts}
		</div>
      </div>
	  
	  </section>
	  </div>
	  </div>
	   </div>
    )
  } // END render

  // the api request function
  fetchApi(url) {

    fetch(url,{ method: 'GET',
               mode: 'cors',
               cache: 'default' }).then((res) => res.json()).then((data) => {
		console.log(data);
      // update state with API data
      this.setState({
        movieID: data.username,
        original_title: data.username,
        tagline: data.name,
        overview: data.bio,
        homepage: data.profilePictureUrlHD,
        poster: data.profilePictureUrlHD,
        production: data.posts,
        production_countries: data.averageLikes,
        genre: data.website,
        followers: data.followers,
        vote: data.totalLikes,
        runtime: data.following,
        revenue: data.averageEngagements,
        backdrop: data.profilePictureUrlHD/*data.mostLikedMedia[0].node.edges.display_url*/,
		mostLiked:data.mostLikedMedia,
		mostComment:data.mostCommentedMedia

      })
    })

    // .catch((err) => console.log('Movie not found!'))

  } // end function

  fetchMovieID(movieID) {
    let url = host + `:3001/socialitycs/${movieID}`
    this.fetchApi(url)
  } // end function
	MyList(items) {
   return (
    <div>
      {items.map((item) => <Post key={item.node.id} data={item} followers={this.state.followers} />)}
    </div>
	)}
	

  componentDidMount() {
    let url = host + `:3001/socialitycs/${this.state.movieID}`
    this.fetchApi(url)

    //========================= BLOODHOUND ==============================//
    let suggests = new Bloodhound({
      datumTokenizer: function(datum) {
		  console.log('datum :'+datum);
        return Bloodhound.tokenizers.whitespace(datum.value);
      },
      queryTokenizer: Bloodhound.tokenizers.whitespace,
      remote: {
        url: 'https://www.instagram.com/web/search/topsearch/?context=blended&query=%QUERY&include_reel=true',
        filter: function(movies) {
			console.log(movies);
          // Map the remote source JSON array to a JavaScript object array
          return $.map(movies.users, function(movie) {
			  if(!movie.user.is_private){
            return {
              value: movie.user.username, // search original title
              id: movie.user.username, // get ID of movie simultaniously
			  thumbnail:movie.user.profile_pic_url
			  };}
          });
        } // end filter
      } // end remote
    }); // end new Bloodhound

    suggests.initialize(); // initialise bloodhound suggestion engine

    //========================= END BLOODHOUND ==============================//

    //========================= TYPEAHEAD ==============================//
    // Instantiate the Typeahead UI
    $('.typeahead').typeahead({
      hint: true,
      highlight: true,
      minLength: 2,
    }, {name: 'datum',
  display: 'value',
  templates: {
    suggestion: function(data) {
    return "<p class='over'><span ><img style=\"width: 42px; height: 42px;\" class='round' src="+data.thumbnail+' alt="Logo" /></span>'+ '</strong> – ' + data.value + '</p>';
}
  },source: suggests.ttAdapter()}).on('typeahead:selected', function(obj, datum) {
      this.fetchMovieID(datum.id)
    }.bind(this)); // END Instantiate the Typeahead UI
    //========================= END TYPEAHEAD ==============================//

  } // end component did mount function

  // } // END CLASS - APP
}
module.exports = App;
