import React from "react";
import { LineChart } from 'react-charts-d3';

const weblog = require('webpack-log');
const log = weblog({ name: 'wds' }) // webpack-dev-server
 
log.info("admin");

var config = require('./../../../config.js').config;
var host = config.host;

function randomNumber() {
  return Math.random() * (40 - 0) + 0;
}


const data = [
  { key: 'Group 1', values: [ { x: 'A', y: randomNumber() }, { x: 'B', y: randomNumber() }, { x: 'C', y: randomNumber() }, { x: 'D', y: randomNumber() } ] },
  { key: 'Group 2', values: [ { x: 'A', y: randomNumber() }, { x: 'B', y: randomNumber() }, { x: 'C', y: randomNumber() }, { x: 'D', y: randomNumber() } ] },
  { key: 'Group 3', values: [ { x: 'A', y: randomNumber() }, { x: 'B', y: randomNumber() }, { x: 'C', y: randomNumber() }, { x: 'D', y: randomNumber() } ] },
];
export default class Settings extends React.Component {
  render() {
    console.log("Analytics");
    
    return (
      <div>

      <section>
      <div class = "card">
      <h2>Indicateurs clés</h2>
      </div>

      </section>


      <section>
      <div class = " card">
      <h2>Evolution des indicateurs</h2>
      <LineChart data={data} />
      </div>

      </section>

      <section>
      <div class = "columns five card">
      <h2> Followers notables </h2>
      
      </div>

      </section>
      </div>
    );
  }
}