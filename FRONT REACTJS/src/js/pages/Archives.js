import React from "react";

import axios from 'axios';
import Cookies from 'universal-cookie'
var config = require('./../../../config.js').config;
var host = config.host;

const Orders = ({ order, index }) =>
  <tr>
    <td>{index + 1}</td>
    <td className="repo-name">{order.shopify_order_id}</td>
    <td>{order.shopify_order_id} ID Shopify</td>
    <td>{order.shopify_order_id}</td>
    <td>{order.shopify_order_id}</td>
  </tr>;


export default class Archives extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      products: [],
      loading: true,
      error: null,
      value: '',
    };
   
  }
 
  componentDidMount() {
    var cookies = new Cookies(cookies);
    var cookie_login = cookies.get('login')
    axios
      .get(
        window.encodeURI(
          host + ':3000/api/orders/'+cookie_login+'',
        ),
      )
      .then(response => {

        const orders = response.data;
		  console.log(response);
        this.setState({
          orders,
          loading: false,
        });
      })
      .catch(error => {
        this.setState({
          error: error,
          loading: false,
        });
      });
  }
  
  renderLoading() {
    return (
      <div>
        Loading...
      </div>
    );
  }


  renderList() {
    const orders = this.state.orders;
    const error=this.state.error;


    return (
	<div>
    <h1>{this.props.title}</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>#</th>
            <th>Order id</th>
            <th>Date</th>
            <th>Aliexpress order id</th>
            <th>Total Price</th>
          </tr>
        </thead>
        <tbody>
          {orders.map((order, index) =>
            <Orders order={order} index={index} key={order.shopify_order_id} />,
          )}
		  
        </tbody>
      </table>
	  <p>{JSON.stringify(this.state)}</p></div>
    );
  }

  render() {
    return this.state.loading ? this.renderLoading() : this.renderList();
  }
}
