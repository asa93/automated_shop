import React from "react";
import Cookies from 'universal-cookie';
import axios from 'axios';

var config = require('./../../../config.js').config;
var host = config.host;

export default class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      instagram_id:"",
      items : [],
      auth : false,
  	
    };
  }
  
   showHide(e){
    e.preventDefault();
    e.stopPropagation();
    this.setState({
      type: 'password',
    })  
  }


  handleChange = event => {
    axios
      .get(
        window.encodeURI(
          host +':3000/login',
        ),
      )
      .then(response => {
        
this.setState({items:response.data});
//	console.log(this.state.items)

      })
if(event.target.className=='login'){
	this.setState({email: event.target.value});
}else {this.setState({
	password:event.target.value
})}

  }
moveSign =()=>{
  this.props.history.push('/sign')
}

handleSubmit = event =>{
	console.log('co',event.target.className);
  console.log(event.target.value)

	// Pour contourner le problème des "this.state" dans la fonction 
	var temp1 = this.state.email;
	var temp2 = this.state.password;
	var AuthBool; // boolean pour savoir si on a réussi à se co 
	var cookies = new Cookies();
  var cookie_insta = new Cookies();
  var insta;
	console.log('temp1',temp1,'\t ',temp2)

this.state.items.filter(function(el){ // Parcours le tableau item ou il y a le resultat de la requete sur dbo.accounts 
console.log("dans la boucle ",el.instagram_id)

  console.log('compte :' + el.shop_id,temp1)

	console.log('pwd :'+ el.shop_pwd)
	console.log("pwd :"+ el.shop_pwd ===temp2)

	if (el.shop_id===temp1 && el.shop_pwd ===temp2){
    // test de connexion et si vrai on affecte le insta dans le cookie 
	AuthBool = true;
  insta = el.instagram_id;
}
	console.log("Authentification :"+AuthBool)
	})
this.state.auth = AuthBool ;
console.log("insta",insta)
	console.log("Constructeur :"+this.state.auth); 

	if (this.state.auth==true){ // si connexion réussi 
		cookies.set('login', this.state.email, { path: '/' });
     // le cookie devient accessible dans toutes les pages du site mais pas dans le serveur backend
     cookie_insta.set('insta',insta,{path:'/'})
		 this.props.history.push('/profil')

	}

}

  render() {

      const containerStyle = { // centrage des élements
    display: 'flex',  
    justifyContent:'center',
	 alignItems:'center', 
	height: '25vh',

    };
    const stylee={
    	marginLeft:'1em',
    }
    const tab={
    	marginLeft:'5em',
    	paddingTop:'0.5em',
    }
    return (
    

      
      <div  style={containerStyle} >
      <section>
		<div class="columns three" ></div>
      <div class =" card columns six">

      <form onSubmit={this.handleSubmit}  >
	  <h3>Connectez vous aya </h3>
      <p style={tab}>Login</p>
      <input type='text' defaultValue={this.state.email} onChange={this.handleChange} className='login' size={25}/>
      <p style={tab}>Mot de passe</p>
      <input type='password' defaultValue={this.state.password} onChange={this.handleChange} size={25} />
 
      <p></p>
      
      <input type='submit' value='Se connecter' class='btn btn-primary'  />

 	    <button class="secondary" onClick={this.moveSign} style={stylee}> Inscrire </button>
      </form>
  
    
      
      </div>
	  <div class="columns three" ></div>
      </section>
      </div>
     
    );

  }
}