import React from "react";
import Cookies from 'universal-cookie';
import axios from 'axios';
// id dbo.accounts : shop_id,shop_pwd,aliexpress_id,aliexpress_pwd,instagram_id,instagram_pwd,
// api_key,api_pwd,token

var config = require('./../../../config.js').config;
var host = config.host;

export default class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    	login :'',
    	login_pwd:'',
    	aliexpress_id:'',
    	aliexpress_pwd:'',
    	instagram_id:'',
    	instagram_pwd:'',
    	api_key:'',
    	api_pwd:'',
    	items:[],
      login_item:[],
      same:false,
    	error:false,
    	error_pwd:false,

    };
  }
    handleChange=event=>{
  	console.log(event.target.className);
  	switch(event.target.className){
  		case 'login' : 
      return this.setState({
  			login:event.target.value,
        error:false,
  		});

  		case 'login_pwd' : 
  		//console.log('le pwd',event.target.value);
  		return this.setState({
  			login_pwd:event.target.value,
        error_pwd:false,
  		});
  		  		case 'insta_id' : 
  		//console.log('le pwd',event.target.value);
  		return this.setState({
  			instagram_id:event.target.value,
  		});
  		  		case 'insta_pwd' : 
  		//console.log('le pwd',event.target.value);
  		return this.setState({
  			instagram_pwd:event.target.value,
  		});
  		  		case 'aliexpress_id' : 
  		//console.log('le pwd',event.target.value);
  		return this.setState({
  			aliexpress_id:event.target.value,
  		});
  		  		case 'aliexpress_pwd' : 
  		//console.log('le pwd',event.target.value);
  		return this.setState({
  			aliexpress_pwd:event.target.value,
  		});
  		  		case 'api_key' : 
  		//console.log('le pwd',event.target.value);
  		return this.setState({
  			api_key:event.target.value,
  		});
  		  		case 'api_pwd' : 
  		//console.log('le pwd',event.target.value);
  		return this.setState({
  			api_pwd:event.target.value,
  		});

  	}
  }
  Insert=()=>{
    //console.log(this.state.same !=true ||this.state.login !="" || this.state.login_pwd != "")
    //console.log(this.state.same !=true && this.state.login !="" && this.state.login_pwd != "")
    if(this.state.same !=true &&this.state.login !="" && this.state.login_pwd != ""  ){
        var item = [];
    console.log(this.state.login)
    // ajout des variables dans l'ordre de la db 
    item.push(this.state.login)
    item.push(this.state.login_pwd)
    item.push(this.state.aliexpress_id)
    item.push(this.state.aliexpress_pwd)
    item.push(this.state.instagram_id)
    item.push(this.state.instagram_pwd)
    item.push(this.state.api_key)
    item.push(this.state.api_pwd)
    var item_string = JSON.stringify(item);
    console.log(item_string)
    console.log("On insère")
    //console.log("On insère dans le db, je ne dois pas voir ça ")

    

    // insertion dans la base de donnée , table accounts 
       axios
      .get(
        window.encodeURI(
          host + ':3000/insert_account/'+item_string+'',
        ),
      )
      .then(response => {
//this.setState({items:response.data});
  console.log(this.state)

      })
  
return true
  }
}
  errorSubmit=(callback)=>{

    // Gestion des erreurs sur le login,le password et si le login est déja pri
        if (this.state.same ==true ||this.state.login =="" || this.state.login_pwd == ""  ){
        // si les champs login et login pwd sont vides, alors on set error ou error_pwd à true 
  // pour qu'il y ait un affichage après le submit
      if (this.state.same == true){
        return  alert("Le shop id est déja utilisé")
      }
      else if (this.state.login=="" && this.state.login_pwd==""){
      
  this.setState({
    error_pwd:true,
    error:true,
  })

  }
  else if (this.state.login_pwd==''){

  this.setState({
    error_pwd:true,
  })
}
      else {
    this.setState({
    
    error:true,
  })

  }
}
return callback ;
}

  getLogin=()=>{


    // Recuperation des comptes dans la base de données
           axios.get(
        window.encodeURI(
          host + `:3000/login`,)
        ,)
      .then(response => {
this.setState(
  {
    login_item:response.data}
  );
  //console.log(this.state.login_item);
  })

}

  handleSubmit=event=>{
     var log=this.state.login;
var trouve=false;
      //this.state.login_item = this.state.login_item.replace(,"");
// condition si on trouve un nom de shop_id identique dans la dbo, alors on set le bool trouve a true sinon false par défaut 
      this.state.login_item.filter(function(el){ // 
  /*console.log(el.shop_id)
  console.log(log)
  console.log("essai1",log==el.shop_id)
    console.log("essai2",log=="test")
      console.log("essai3","test"==el.shop_id)
    console.log("test"=="test")*/
  if (el.shop_id===log){
  trouve=true;
    }


  })
 this.state.same=trouve
//console.log(this.errorSubmit(this.Insert()))
// insertion si pas d'erreur
 if ( this.errorSubmit(this.Insert()) == true)
  {this.props.history.push('/')}

}

  

  render(){
    // pour pouvoir effectuer la requete dès le chargement sinon,
    // quand on fait un submit sur l'inscription, il ne détecte pas si le login est déja pris la première fois 
    this.getLogin();


  	const tabstyle = {
  		marginLeft:'0em',
  		fontWeight:'bold',
  	}
  	      const containerStyle = { // centrage des élements
    display: 'flex',  
    justifyContent:'center',
	 alignItems:'center', 
	

    };
    var error_log =this.state.login;
  	return( 
  		<div style={containerStyle}>
  		<form onSubmit={this.handleSubmit} >
  		
  		
  			<p style={tabstyle}>Login</p>
  		<input type='text' className='login'  onChange={this.handleChange}/>
  		{this.state.error?"Erreur sur le login":""}
  			<p style={tabstyle}>Password Login</p>
  		
  		<input type='text' className='login_pwd'  onChange={this.handleChange}/>
  		{this.state.error_pwd?"Erreur sur le mot de passe":""}
  		  		<p style={tabstyle}>Login instagram</p>
  		<input type='text' className='insta_id'  onChange={this.handleChange}/>
  		  		<p style={tabstyle}>Password instagram</p>
  		<input type='text' className='insta_pwd'  onChange={this.handleChange}/>
  	 <p></p>
  		<input type='submit' value='Sign' style={tabstyle} class='btn btn-primary'/>
  		
  	</form>
  	</div>
)
  }
}