''' @Slimane ASA'''
import time
from instapkg.instapyDB import *

BOTNAME = "un_deux_trois_bijoux"
PASSWORD = "mdp123456"

class InstaP:
    """Contains Everything About A Given Instagramer"""
    i = 12345

    def __init__(self, username="JohnDoe", publications=0, abonnes=0, suivis=0):
        self.username = username
        self.posts = publications
        self.following = suivis
        self.followers = abonnes
        
    def __str__(self):
         return self.username + " : " + str(self.posts) + " publications - " + str(self.followers) + " abonnes - " + str(self.following) + " suivis"
    
    def toCsv(self):
         return self.username + ";" + str(self.posts) + ";" + str(self.followers) + ";" + str(self.following)

def toCsv(instaList, file):
    ''' Save InstaP Data to CSV'''
    with open(file, 'w', newline='') as output:
        output.write("username;posts;followers;following" + "\n")
        for instaP in instaList:
            output.write(instaP.toCsv() + "\n")  


def follow(actions,driver, username):
    driver.get("https://www.instagram.com/" + username)
    buttons = driver.find_elements_by_tag_name('button')
    followButton = buttons[0]
    followButton.click() #vérifier que la personne n'est pas déjà follow!
    
    actions.append(BotAction(datetime.datetime.now(), BotAction.BOT_FOLLOW_PERSON, username))
    
    
    return actions

def login(driver, username = BOTNAME, password = PASSWORD):
    # Load page
    driver.get("https://www.instagram.com/accounts/login/")

    # Login
    driver.find_element_by_xpath("//div/input[@name='username']").send_keys(username)
    driver.find_element_by_xpath("//div/input[@name='password']").send_keys(password)
    driver.find_element_by_xpath("//span/button").click()


def getBasicData(driver, instaP):
    username = instaP.username
    driver.get("https://www.instagram.com/"+ username + "/")
    
    time.sleep(1)

    following = ""
    followers = ""
    posts = ""
    userExist = 1
    
    while((following == "" or followers == "" or posts == "") and userExist): #on attend que ça soit chargé
        time.sleep(0.5)
        elts = driver.find_elements_by_xpath("//section/ul/li/a")
        
        for elt in elts:
            if("following" in elt.text):
                following = elt.text.replace(",","").replace("m","").replace(".","").replace(" following","")
            elif("follower" in elt.text):
                followers = elt.text.replace(",","").replace("m","").replace(".","").replace(" followers","")
            elif("post" in elt.text):
                posts = elt.text.replace(",","").replace("m","").replace(".","").replace(" posts","")              
            
        elts = driver.find_elements_by_xpath("//section/ul/li/span")
        
        for elt in elts:
            if("following" in elt.text):
                following = elt.text.replace(",","").replace("m","").replace(".","").replace(" following","")
            elif("follower" in elt.text):
                followers = elt.text.replace(",","").replace("m","").replace(".","").replace(" followers","").replace(" follower","")
            elif("post" in elt.text):
                posts = elt.text.replace(",","").replace("m","").replace(".","").replace(" posts","").replace(" post","")
                
        #let's check the page is working, sometimes it's not so let's move on
        elts = driver.find_elements_by_xpath("//div/div/div/div/h2")
        for elt in elts:
            if("isn't available" in elt.text):
                userExist = 0
            else:
                userExist = 1
                
                

        
    instaP.following = following
    instaP.followers = followers
    instaP.posts = posts
                

def followFollowers(actions,driver,username,action_limit):
    #1st STEP POP UP FOLLOWERS WINDOW
    driver.get("https://www.instagram.com/"+ username + "/")

    time.sleep(3)
    
    usernames = []
    
    links = driver.find_elements_by_tag_name('a')
    for l in links :
        if l.get_attribute('href')=="https://www.instagram.com/"+username+"/followers/" :
            print(l.get_attribute('href'))
            followersButton = l

    followersButton.get_attribute('href')

    followersButton.click()
    
    time.sleep(5)
    
    #2nd STEP SCROLL FOLLOWERS
    followers = []


    SCROLL_PAUSE = 0.5
     
    #driver.execute_script("followersbox = document.getElementsByClassName('_gs38e')[0];")
    
    
    #On scroll pour avoir suffisamment de person à follow (de boutons du coup) ou jusqu'à ce qu'on ait atteint le fond de la boîte de scroll
    
    
    
    #_________________________BOUCLE DE SCROLLING 
    buttons = []
    driver.execute_script("followersbox = document.querySelectorAll('div > div > div > div[role=\"dialog\"] > div')[1];");
    buttons = driver.find_elements_by_xpath("//div/div/ul/div/li/div/div/button")
    while len(buttons) > 11 and len(actions) <= action_limit :
        
        
       
        buttons = driver.find_elements_by_xpath("//div/div/ul/div/li/div/div/button")
        print(len(buttons))
        cases= driver.find_elements_by_xpath("//div/div/ul/div/li")
        print(len(cases))
        
        #On récupère les usernames pour pouvoir les rentrer dans la DB
        usernames= driver.find_elements_by_xpath("//div/div/ul/div/li/div/div/div/div/a")
        for b in buttons:
            i = buttons.index(b)
            print(i)
            if i==0:
                driver.execute_script("followersbox = document.querySelectorAll('div > div > div > div[role=\"dialog\"] > div')[1];");
                driver.execute_script("followersbox.scrollTo(0, followersbox.scrollHeight);")
           
            #____________________INSERT CODE HERE
            #On ne followera pas tout le monde car certains sont déjà follow sans doute..
            while b.text == "Follow" :
                if(b.text == "Follow"):
                    b.click()

                    time.sleep(5)
                    
                    
                    print(usernames[i].get_attribute('title'))
                    if(b.text != "Follow"):
                        actions.append(BotAction(datetime.datetime.now(), BotAction.BOT_FOLLOW_PERSON, usernames[i].get_attribute('title')))
                        print(len(actions))
                    elif(b.text == "Follow"):
                        #pause bot action follow is blocked by instagramm
                        
                        time.sleep(30)
            #___________________________END CODE
            js = "var li=document.querySelectorAll('div > div > ul > div > li')[0];li.parentNode.removeChild(li)"
            driver.execute_script(js)
            time.sleep(1)
            if(len(actions) >= action_limit):
                break
            
    

        
    return actions
    
def updateFollowers2(actions,driver,username,dbusers,action_limit):
    
    driver.get("https://www.instagram.com/"+ username + "/")
    
    usernames = []
    
    links = driver.find_elements_by_tag_name('a')
    for l in links :
        if l.get_attribute('href')=="https://www.instagram.com/"+username+"/followers/" :
            print(l.get_attribute('href'))
            followersButton = l

    followersButton.get_attribute('href')
    
    time.sleep(10)
    followersButton.click()
    
    time.sleep(5)
    
    #2nd STEP SCROLL FOLLOWERS
    followers = []


    SCROLL_PAUSE = 0.5
     
    #driver.execute_script("followersbox = document.getElementsByClassName('_gs38e')[0];")
    
    
    #On scroll pour avoir suffisamment de person à follow (de boutons du coup) ou jusqu'à ce qu'on ait atteint le fond de la boîte de scroll
    
    
    buttons = []
    #access dialog box
    driver.execute_script("followersbox = document.querySelectorAll('div > div > div > div[role=\"dialog\"] > div')[1];");
    

    buttons = driver.find_elements_by_xpath("//div/div/ul/div/li/div/div/button")
    while len(buttons) > 11 and len(actions) <= action_limit :
        
        
       
        buttons = driver.find_elements_by_xpath("//div/div/ul/div/li/div/div/button")
        print(len(buttons))
        cases= driver.find_elements_by_xpath("//div/div/ul/div/li")
        print(len(cases))
        
        #On récupère les usernames pour pouvoir les rentrer dans la DB
        usernames= driver.find_elements_by_xpath("//div/div/ul/div/li/div/div/div/div/a")
        for b in buttons:
            i = buttons.index(b)
            print(i)
            if i==0:
                driver.execute_script("followersbox = document.querySelectorAll('div > div > div > div[role=\"dialog\"] > div')[1];");
                driver.execute_script("followersbox.scrollTo(0, followersbox.scrollHeight);")
           
            #____________________COODE HERE
            #On ne followera pas tout le monde car certains sont déjà follow sans doute..
 

            #___________________________END CODE
            js = "var li=document.querySelectorAll('div > div > ul > div > li')[0];li.parentNode.removeChild(li)"
            driver.execute_script(js)
            time.sleep(1)
            if(len(actions) >= action_limit):
                break

    return actions
   
def updateFollowers(actions,driver,username,dbuserss):
    'Check followers list for a given account in order to update DB accordingly'
    dbusers=dbuserss
    toUnfollow =[]
    
    #unfollow=DBGetUnfollow()
    driver.get("https://www.instagram.com/"+ username + "/")
    
    links = driver.find_elements_by_tag_name('a')
    
    #access account list of followers
    for l in links :
        if l.get_attribute('href')=="https://www.instagram.com/"+username+"/followers/" :
            l.click()
    
    #scroll the list to get data
    
    linkss = driver.find_elements_by_tag_name('a')
    beforeLen=0
    while len(linkss)>beforeLen :
        beforeLen=len(linkss)
        driver.execute_script("followersbox = document.querySelectorAll('div > div > div > div[role=\"dialog\"] > div')[1];");

        driver.execute_script("followersbox.scrollTo(0, followersbox.scrollHeight);")
        
        time.sleep(0.1) #on attend que ça charge
        linkss = driver.find_elements_by_tag_name('a')
    #NEW FOLLOWERS
    
    for l  in linkss:
        username = l.get_attribute('title')
        toUnfollow.append(username)
        #add new username to DB
        
        if username != "" and str(username) not in dbusers:
            actions.append(BotAction(datetime.datetime.now(), BotAction.PERSON_FOLLOW_BOT, username))
            
    
    for dbuser  in dbusers:
        if dbuser not in toUnfollow:
            actions.append(BotAction(datetime.datetime.now(), BotAction.PERSON_UNFOLLOW_BOT,dbuser))
    
    
    return actions
    
    
def cleaning(actions,driver,username,dbusers):
    unfollow=dbusers
    print(unfollow)
    driver.get("https://www.instagram.com/"+ username + "/")
    links = driver.find_elements_by_tag_name('a')
    
    for l in links :
        if l.get_attribute('href')=="https://www.instagram.com/"+username+"/following/" :
            l.click()
    
        
   
        
        
       
        
    elts = driver.find_elements_by_xpath("//li/div/div/button")
    print(len(elts))
    cases= driver.find_elements_by_xpath("//div/div/ul/div/li")
    print(len(cases))
    
    #On récupère les usernames pour pouvoir les rentrer dans la DB
    usernames= driver.find_elements_by_xpath("//div/div/ul/div/li/div/div/div/div/a")
  
    #3rd STEP GET USERNAMES
    i=0
    while len(elts) > 11 :
        elts = driver.find_elements_by_xpath("//li/div/div/button")
        linksss = driver.find_elements_by_xpath("//li/div/div/div/div/a")
        cases= driver.find_elements_by_xpath("//div/div/ul/div/li")
        print(len(cases))
        
        for elt in range(0,len(elts)):
            
            if elt==0:
                driver.execute_script("followersbox = document.querySelectorAll('div > div > div > div[role=\"dialog\"] > div')[1];");
                driver.execute_script("followersbox.scrollTo(0, followersbox.scrollHeight);")
            
            title=linksss[elt].get_attribute('title')
            if(str(title) in unfollow):
                if(elts[elt].text == "Following"):
                    elts[elt].click()
                    unfo=driver.find_elements_by_xpath("//div/div/div/div/div/button")[0]
                    time.sleep(0.3)
                    unfo.click()
                    i=i+1
                    actions.append(BotAction(datetime.datetime.now(), BotAction.BOT_UNFOLLOW_PERSON, title))
                    time.sleep(1)
                    if i == 15 :
                        i=0
                        time.sleep(900)
            js = "var li=document.querySelectorAll('div > div > ul > div > li')[0];li.parentNode.removeChild(li)"
            driver.execute_script(js)
            time.sleep(0.5)
            
        
    return actions

def CommentOnPostByUsername(actions,username,driver,comment):
    driver.get("https://www.instagram.com/"+ username + "/")
    #Links seulenment sur les nombres impaires
    Posts = driver.find_elements_by_xpath("//main/div/div/article/div/div/div/div/a/div[@role='button']/div")
    Posts[1].click()
    time.sleep(1)
    com=driver.find_elements_by_xpath('//textarea[@placeholder = "Add a comment…"]')
    actions_ = webdriver.ActionChains(driver)
    actions_.move_to_element(com[0])
    actions_.click()
    actions_.send_keys(comment)
    actions_.send_keys(Keys.RETURN)
    actions_.perform()
    
    return actions

def CommentOnPostByTag(actions,tag,driver,comment,nbmin,top):
    driver.get("https://www.instagram.com/explore/tags/"+ tag + "/")
    #Links seulement sur les nombres impaires
    #top post //main/article/div/div/div/div/div/a/div[@role='button']/div
    if top==1:
        Posts = driver.find_elements_by_xpath("//main/article/div/div/div/div/div/a/div[@role='button']/div")
    elif top==0:
        #most recent //main/article/div/div/div/div/a/div[@role='button']/div
        Posts = driver.find_elements_by_xpath("//main/article/div/div/div/div/a/div[@role='button']/div")
        
    Posts[9].click()
    time.sleep(1)
    #number of likes //div/section/div/a/span
    nblikes= driver.find_elements_by_xpath("//div/section/div/a[@role='button']/span")
    if(len(nblikes)>0):
        #if an amount is displayed then clean and make it a number
        nb=int(nblikes[0].text.replace(",", ""))
        
    else:
        #not enough person to like means no amount displayed
        nb=0
        
     
    if nbmin <= nb :
        #get like element
        like=driver.find_elements_by_xpath("//span[@aria-label='Like']")
        #action if not already liked
        if(len(like)>0):
            action = webdriver.ActionChains(driver)
            action.move_to_element(like[0])
            action.click()
            action.perform()
        #Actions to comment
        com=driver.find_elements_by_xpath('//textarea[@placeholder = "Add a comment…"]')
        actions = webdriver.ActionChains(driver)
        actions.move_to_element(com[0])
        actions.click()
        actions.send_keys(comment)
        actions.send_keys(Keys.RETURN)
        actions.perform()
        
        return actions
def unfollowAll(driver,username):
    driver.get("https://www.instagram.com/"+ username + "/")
    time.sleep(1)
    links = driver.find_elements_by_tag_name('a')
    
    for l in links :
        if l.get_attribute('href')=="https://www.instagram.com/"+username+"/following/" :
            l.click()
    elts = driver.find_elements_by_xpath("//li/div/div/button")
    i=0
    while len(elts) > 11 :
        elts = driver.find_elements_by_xpath("//li/div/div/button")
        linksss = driver.find_elements_by_xpath("//li/div/div/div/div/a")
        cases= driver.find_elements_by_xpath("//div/div/ul/div/li")
        for elt in range(0,len(elts)):
                
            if elt==0:
                driver.execute_script("followersbox = document.querySelectorAll('div > div > div > div[role=\"dialog\"] > div')[1];");
                driver.execute_script("followersbox.scrollTo(0, followersbox.scrollHeight);")
            
            title=linksss[elt].get_attribute('title')
            if(elts[elt].text == "Following"):
                elts[elt].click()
                i=i+1
                print(i)
                unfo=driver.find_elements_by_xpath("//div/div/div/div/div/button")[0]
                time.sleep(0.2)
                unfo.click()
                time.sleep(1)
                if i == 15 :
                        i=0
                        time.sleep(900)
            js = "var li=document.querySelectorAll('div > div > ul > div > li')[0];li.parentNode.removeChild(li)"
            driver.execute_script(js)
            time.sleep(1)