
/**
 * @file contain all functions that interact with main DB
 * @author Slimane & Pierre
 */


var config = require('./config.json');
config = config.DB_DEV;


var sql = require('mssql');



exports.get_products_info = function(prodId, callback) {
  //4.

var Result=[];
    var dbConn = new sql.ConnectionPool(config);
    //5.
    dbConn.connect().then(function () {
        //6.
        
        var requests = new sql.Request(dbConn);
        //7.
        requests.query("select * from product where shopify_product_id in "+ prodId).then(function (recordSet) {
            
            
            dbConn.close();
            sql.close(); 
            return callback(JSON.stringify(recordSet.recordset));
            
        }).catch(function (err) {
            //8.
            console.log(err);
            dbConn.close();
        });
    }).catch(function (err) {
        //9.
        console.log(err);
    });
};



/**
 * Retrieve product lists for a given shop
 * @constructor
 * @param {string} shopifyID - ID of the shop we need to retrieve products for
 * @param {string} callback - Callback function to send results to
 *@return {boolean} success : True / False
 */

 exports.get_products = function(shopifyID, callback) {
  //return the list of products from DB for a given shop if


var Result=[];
    var dbConn = new sql.ConnectionPool(config);
    //5.
    dbConn.connect().then(function () {
        //6.
        
        var requests = new sql.Request(dbConn);
        //7.
        requests.query("select * from product where shop_id = '" + shopifyID +"'").then(function (res) {
            
            
            dbConn.close();
            sql.close(); 
            return callback(res.recordset);
            
        }).catch(function (err) {
            //8.
            console.log(err);
            dbConn.close();
        });
    }).catch(function (err) {
        //9.
        console.log(err);
    });
};


/**
 * Retrieve product from an id and a shop_id
 * @constructor
 * @param {string} shop_id - ID of the shop we need to retrieve products for
 * @param {string} shopify_product_id - 
 *@return {json} product : True / False
 */

 exports.get_product = function(shop_id, shopify_product_id, callback) {


var Result=[];
    var dbConn = new sql.ConnectionPool(config);
    //5.
    dbConn.connect().then(function () {
        //6.
        
        var requests = new sql.Request(dbConn);
        //7.
        requests.query("select * from product where shop_id = '" + shop_id + "' and shopify_product_id = '"+ shopify_product_id +"'").then(function (res) {
            
            
            dbConn.close();
            sql.close(); 
            return callback(res.recordset[0]);
            
        }).catch(function (err) {
            //8.
            console.log(err);
            dbConn.close();
        });
    }).catch(function (err) {
        //9.
        console.log(err);
    });
};


/**
 * Retrieve all orders for a given shop
 * @constructor
 * @param {string} shopifyID - ID of the shop we need to retrieve products for
 * @param {string} callback - Callback function to send results to
  *@return {boolean} success : True / False
 */

 exports.get_orders = function(shopifyID, callback) {
  //return the list of products from DB for a given shop if


var Result=[];
    var dbConn = new sql.ConnectionPool(config);
    //5.
    dbConn.connect().then(function () {
        //6.
        
        var requests = new sql.Request(dbConn);
        //7.
        requests.query("select * from [dbo].[order] where shopify_account_id = '"+  shopifyID +"'").then(function (res) {
            
            
            dbConn.close();
            sql.close(); 
            return callback(res.recordset);
            
        }).catch(function (err) {
            //8.
            console.log(err);
            dbConn.close();
        });
    }).catch(function (err) {
        //9.
        console.log(err);
    });
};



exports.get_new_orders = function(orders,callback) {

var Result=[];
    var dbConn = new sql.ConnectionPool(config);
    //5.
    dbConn.connect().then(function () {
        //6.
    
        var requests = new sql.Request(dbConn);
        //7.
        requests.query("select shopify_order_id from [dbo].[order]").then(function (recordSet) {
    
      
            dbConn.close();
      sql.close(); 
      var ordersJSON={};
      var key = 'order';
      ordersJSON[key] = []
      
        var ids=[];
        for( var j=0;j<orders.length;j++){
          ids.push(orders[j].id);
        }
      console.log(ids);
      
      for(var i=0; j=recordSet.recordset.length,i<j; i++){ 

      var index=ids.indexOf(parseInt(recordSet.recordset[i].shopify_order_id))
     
       if(index>=0 ){
         ids.splice(index,1);};
       
       }
       console.log(ids);
      return callback(ids);
      
        }).catch(function (err) {
            //8.
            console.log(err);
            dbConn.close();
        });
    }).catch(function (err) {
        //9.
        console.log(err);
    });
};


exports.get_new_products = function(products,callback) {

var Result=[];
    var dbConn = new sql.ConnectionPool(config);
    //5.
    dbConn.connect().then(function () {
        //6.
    
        var requests = new sql.Request(dbConn);
        //7.
        requests.query("select shopify_product_id from [dbo].[product]").then(function (recordSet) {
    
      
            dbConn.close();
      sql.close(); 
      var productsJSON={};
      var key = 'product';
      productsJSON[key] = []
      
        var ids=[];
        for( var j=0;j<products.length;j++){
          ids.push(products[j].id);
        }
      console.log(ids);
      
      for(var i=0; j=recordSet.recordset.length,i<j; i++){ 

      var index=ids.indexOf(parseInt(recordSet.recordset[i].shopify_product_id))
     
       if(index>=0 ){
         ids.splice(index,1);};
       
       }
       console.log(ids);
      return callback(ids);
      
        }).catch(function (err) {
            //8.
            console.log(err);
            dbConn.close();
        });
    }).catch(function (err) {
        //9.
        console.log(err);
    });
};

/**
 * Insert a product in the database
 * @constructor
* @param {string} product.shopify_product_id
* @param {string} product.shop_id
* @param {string} product.aliexpress_product_id
* @param {real} product.sell_price
* @param {real} product.buy_price
* @param {boolean} product.stock
* @param {json} product.variant
 */
exports.insert_products = function(product, callback){

    var parameters = [
        { name: 'shopify_product_id', sqltype: sql.NVarChar, value: product['shopify_product_id']},
        { name: 'shop_id', sqltype: sql.NVarChar, value: product['shop_id']},
        { name: 'aliexpress_product_id', sqltype: sql.NVarChar, value: product['aliexpress_product_id']},
        { name: 'name', sqltype: sql.NVarChar, value: product['name']},
        { name: 'sell_price', sqltype: sql.NVarChar, value: product['sell_price']},
        { name: 'buy_price', sqltype: sql.NVarChar, value: product['buy_price']},
        { name: 'stock', sqltype: sql.NVarChar, value: product['stock']},
        { name: 'variant', sqltype: sql.NVarChar, value: product['variant']},,     
    ];

    var Result=[];
    var dbConn = new sql.ConnectionPool(config);
    //5.
    dbConn.connect().then(function () {
    //6.

    var requests = new sql.Request(dbConn);
    //7.
    parameters.forEach(function(p) {
    requests.input(p.name, p.sqltype, p.value);
    });
    requests.query("INSERT INTO product (shopify_product_id,shop_id,aliexpress_product_id,name,sell_price,buy_price,stock,variant) VALUES (@shopify_product_id,@shop_id,@aliexpress_product_id,@name,@sell_price,@buy_price,@stock,@variant)").then(function (recordSet) {


    dbConn.close();
    sql.close();

    return callback('win');

    }).catch(function (err) {
    //8.
    console.log(err);
    dbConn.close();
    sql.close();
    });
    }).catch(function (err) {
    //9.
    console.log(err);
    });
}
/**
 * Update a product in the database
 * @constructor
* @param {string} product.shopify_product_id
* @param {string} product.shop_id
* @param {string} product.name
* @param {string} product.aliexpress_product_id
* @param {real} product.sell_price
* @param {real} product.buy_price
* @param {boolean} product.stock
 */

exports.update_product =function (productInfo) {
  //4.
  //A REPARER 

var Result=[];
    var dbConn = new sql.ConnectionPool(config);
    //5.
    dbConn.connect().then(function () {
        //6.
        
        var requests = new sql.Request(dbConn);
        
        requests.query("Update product set aliexpress_product_id = " + (productInfo.aliexpressId== ''  ? 0 : productInfo.aliexpressId)+", [sell_price]="+(productInfo.sell_price== ''  ? 0 : productInfo.sell_price)+" ,[buy_price]="+(productInfo.buy_price== ''  ? 0 : productInfo.buy_price)+" ,[stock]="+(productInfo.stock== ''  ? 0 : productInfo.stock)+" where shopify_product_id="+productInfo.shopifyProductId).then(function (recordSet) {
        
            
            dbConn.close();
            sql.close();
            
            console.log('win');
            
        }).catch(function (err) {
            //8.
            console.log(err);
            dbConn.close();
            sql.close();
        });
    }).catch(function (err) {
        //9.
        console.log(err);
    });
};


/**
 * Update the variant of a specific product in the database
 * @constructor
* @param {string} productInfo[0].shopifyProductId - Shopify product ID
* @param {string} productInfo[1] - JSON of the VARIANT
* @param {JSON} productInfo - Post query sent by the front end
 */
exports.update_variant =function (productInfo) {
  //4.
  //A REPARER 
 
var Result=[];
    var dbConn = new sql.ConnectionPool(config);
    //5.
    dbConn.connect().then(function () {
        //6.
        
        var requests = new sql.Request(dbConn);
         var parameters = [
        { name: 'shopify_product_id', sqltype: sql.NVarChar, value: productInfo[0].shopifyProductId},
        { name: 'variant', sqltype: sql.NVarChar, value: productInfo[1]}    
        ];
        parameters.forEach(function(p) {
        requests.input(p.name, p.sqltype, p.value);
    });
        requests.query("Update product set [variant]=@variant where shopify_product_id=@shopify_product_id").then(function (recordSet) {
        
            
            dbConn.close();
            sql.close();
            
            console.log('win');
            
        }).catch(function (err) {
            //8.
            console.log(err);
            dbConn.close();
            sql.close();
        });
    }).catch(function (err) {
        //9.
        console.log(err);
    });
};

/**
 * Insert orders in the database
 * @constructor

* @param {Object} order 
 */
 exports.insert_orders = function(order,callback) {
  //4.
    var parameters = [
      { name: 'toto', sqltype: sql.NVarChar, value: order},
      { name: 'account', sqltype: sql.NVarChar, value: 'krist-fit'}
    ];
    console.log('Insertion ID: '+order);

var Result=[];
    var dbConn = new sql.ConnectionPool(config);
    //5.
    dbConn.connect().then(function () {
        //6.
        
        var requests = new sql.Request(dbConn);
        //7.
          parameters.forEach(function(p) {
            requests.input(p.name, p.sqltype, p.value);
        });
        requests.query("INSERT INTO [order] (shopify_order_id,shopify_account_id) VALUES (@toto,@account)").then(function (recordSet) {
        
            
            dbConn.close();
            sql.close();
            
            return callback('win');
            
        }).catch(function (err) {
            //8.
            console.log(err);
            dbConn.close();
            sql.close();
        });
    }).catch(function (err) {
        //9.
        console.log(err);
    });
};

 /**
 * Return credit card info for a given shop
 * @constructor
* @param {string} shop_id 
 */
exports.get_credit_card = function(shop_id, callback){

var Result=[];
    var dbConn = new sql.ConnectionPool(config);
    //5.
    dbConn.connect().then(function () {
        //6.
        
        var requests = new sql.Request(dbConn);
        //7.
        requests.query("select * from [dbo].[credit_card] where shop_id = '"+  shop_id +"'").then(function (res) {
            
            
            dbConn.close();
            sql.close(); 
            return callback(res.recordset);
            
        }).catch(function (err) {
            //8.
            console.log(err);
            dbConn.close();
        });
    }).catch(function (err) {
        //9.
        console.log(err);
    });

}
/*
Recupère dans toute la table dbo.accounts, les attributs shop_id et shop_pwd 
*/
exports.get_accounts = function( callback){

var Result=[];
    var dbConn = new sql.ConnectionPool(config);
    //5.
    dbConn.connect().then(function () {
        //6.
        
        var requests = new sql.Request(dbConn);
        //7.
        requests.query("select shop_id,shop_pwd,instagram_id from [dbo].[accounts] ").then(function (res) {
            
            
            dbConn.close();
            sql.close(); 
            return callback(res.recordset);
            
        }).catch(function (err) {
            //8.
            console.log(err);
            dbConn.close();
        });
    }).catch(function (err) {
        //9.
        console.log(err);
    });
}

/*
Recupère toutes les données dans dbo.accounts sauf le shop_pwd
*/
exports.get_profile = function( shop_id,callback){

var Result=[];
    var dbConn = new sql.ConnectionPool(config);
    //5.
    dbConn.connect().then(function () {
        //6.
        
        var requests = new sql.Request(dbConn);
        //7.
        requests.query("SELECT shop_id,aliexpress_id,aliexpress_pwd,instagram_id,instagram_pwd,api_key,api_pwd,token FROM [dbo].[accounts] where shop_id = '"+  shop_id +"'")
        .then(function (res) {
                
            dbConn.close();
            sql.close(); 
            return callback(res.recordset);
            
      }).catch(function (err) {
            //8.
            console.log(err);
            dbConn.close();
        });
    }).catch(function (err) {
        //9.
        console.log(err);
    });
}
/*
Insert in dbo.account or creating a new account in the database

*/
exports.insert_account = function(product,callback){
    // utilisation de regex pour rendre le resultat en une string ou on peut split et avoir un tableau correcte sans des "" ou []
    result = product.replace(/"/g,"");
    result = result.replace(/\[/g,"");
    result = result.replace(/\]/g,"");
    result = result.split(",");
    console.log("le console ",result);
    var param = []
     result.filter(function(el){ // Parcours le tableau item ou il y a le resultat de la requete sur dbo.accounts 
        param.push(el);
})
     console.log(param[0])
    var Result=[];
    var dbConn = new sql.ConnectionPool(config);
    //5.
    dbConn.connect().then(function () {
    //6.

    var requests = new sql.Request(dbConn);
    //7.

    requests.query("INSERT INTO [dbo].[accounts] (shop_id,shop_pwd,aliexpress_id,aliexpress_pwd,instagram_id,instagram_pwd,api_key,api_pwd) VALUES ('"+param[0]+"','"+param[1]+"','"+param[2]+"','"+param[3]+"','"+
        param[4]+"','"+param[5]+"','"+param[6]+"','"+param[7]+"')")
    .then(function (recordSet) {



    dbConn.close();
    sql.close();

    return callback('Insert');

    }).catch(function (err) {
    //8.
    console.log(err);
    dbConn.close();
    sql.close();
    });
    }).catch(function (err) {
    //9.
    console.log(err);
    });
}
/*
Recupère toutes les données dans dbo.routine liée à l'utilisateur donnée en param
*/
exports.get_routine = function( shop_id,callback){

var Result=[];
    var dbConn = new sql.ConnectionPool(config);
    //5.
    dbConn.connect().then(function () {
        //6.
        
        var requests = new sql.Request(dbConn);
        //7.
        requests.query("SELECT * FROM [dbo].[routine] where shop_id = '"+  shop_id +"'")
        .then(function (res) {
                
            dbConn.close();
            sql.close(); 
            return callback(res.recordset);
            
      }).catch(function (err) {
            //8.
            console.log(err);
            dbConn.close();
        });
    }).catch(function (err) {
        //9.
        console.log(err);
    });
}
exports.get_routineaction = function( routine_id,callback){

var Result=[];
    var dbConn = new sql.ConnectionPool(config);
    //5.
    dbConn.connect().then(function () {
        //6.
        
        var requests = new sql.Request(dbConn);
        //7.
        requests.query("SELECT * FROM [dbo].[botAction] where routine_id = '"+  routine_id +"'")
       
        .then(function (res) {
                
            dbConn.close();
            sql.close(); 
            return callback(res.recordset);
            
      }).catch(function (err) {
            //8.
            console.log(err);
            dbConn.close();
        });
    }).catch(function (err) {
        //9.
        console.log(err);
    });
}

exports.get_routine_nbAction = function( shop_id,callback){

var Result=[];
    var dbConn = new sql.ConnectionPool(config);
    //5.
    dbConn.connect().then(function () {
        //6.
        
        console.log(`SELECT COUNT([botAction].[date]) as nbAction, [routine].[shop_id]
      ,[routine].[routine_id]
      ,[routine_name]
      ,[start_date]
      ,[end_date]
  FROM [dbo].[routine]
  LEFT JOIN [dbo].[botAction] on [botAction].[routine_id] = [routine].[routine_id] 
  WHERE [routine].[shop_id] = '` + shop_id + `'  
  GROUP BY [routine].[routine_id], [routine].[shop_id]
        ,[routine_name]
      ,[start_date]
      ,[end_date]
`);
        var requests = new sql.Request(dbConn);
        //7.
        requests.query(`
SELECT COUNT([botAction].[date]) as nbAction, [routine].[shop_id]
      ,[routine].[routine_id]
      ,[routine_name]
      ,[start_date]
      ,[end_date]
  FROM [dbo].[routine]
  LEFT JOIN [dbo].[botAction] on [botAction].[routine_id] = [routine].[routine_id]
  WHERE [routine].[shop_id] = '` + shop_id +`'
  GROUP BY [routine].[routine_id], [routine].[shop_id]
        ,[routine_name]
      ,[start_date]
      ,[end_date]
`)
       
        .then(function (res) {
                
            dbConn.close();
            sql.close(); 
            return callback(res.recordset);
            
      }).catch(function (err) {
            //8.
            console.log(err);
            dbConn.close();
        });
    }).catch(function (err) {
        //9.
        console.log(err);
    });
}