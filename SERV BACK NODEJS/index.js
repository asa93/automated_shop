/**
 * @file contain all routes to back end
 * @author Slimane & Pierre
 */



const dotenv = require('dotenv').config();
const express = require('express');
const app = express();
const crypto = require('crypto');
const cookie = require('cookie');
const nonce = require('nonce')();
const querystring = require('querystring');
const request = require('request-promise');


const apiKey = process.env.SHOPIFY_API_KEY;
const apiSecret = process.env.SHOPIFY_API_SECRET;
const scopes = 'read_products,read_orders';
const cors = require('cors')


const u_db_query = require('./u_db_query');


var config = require('./config.json');

const forwardingAddress = config.NGROK_URL; 
const shopifyAccessToken = config.SHOPIFY_ACCESS_TOKEN;

const aliexpress_id = config.ALIEXPRESS_ID;
const aliexpress_pwd = config.ALIEXPRESS_PWD;



var head={
  'X-Shopify-Access-Token': shopifyAccessToken,
};


var bodyParser = require('body-parser')
app.use(bodyParser.json())
var http = require('http');
var schedule = require('node-schedule');



// TEST RECUPERATION VARIANTE
  request.get('https://krist-fit.myshopify.com/admin/products.json', { headers: head , json : true})
  .then((response) => 
  {
    variantes = response['products'][1]['variants'];

      variante = variantes.filter(
      function(variantes){
        if(variantes.option1 == "M(EU)" && variantes.option2 == "Bleu") {return "ok" }
      }
      );

      //console.log(variante);


    });

  request.get('https://krist-fit.myshopify.com/admin/orders.json', { headers: head , json : true})
  .then((response) => 
  {
    order = response['orders'][0];
    product = order['line_items'][0];

      variant_id = product['variant_id'];
      request.get('https://krist-fit.myshopify.com/admin/api/2019-04/variants/'+ variant_id +'.json', { headers: head , json : true})
      .then((result) => 
      {

        console.log(result.variant.option1, result.variant.option2);


      });


    });




// SECHEDULED JOB
/*
var jUpdateOrders = schedule.scheduleJob('* /5 * * * * *', function(){
 

	var date = new Date();
			
	try{
	request.get('https://krist-fit.myshopify.com/admin/orders.json', { headers: head , json : true})
	.then((response) => 
	{
	  var orders = response.orders;
	  u_db_query.get_new_orders(orders, function(result){
		  console.log('insertion : '+ result);
		  if(result!=''){
    		 // u_db_query.insert_orders(result,function(p){
    		 	// console.log('Réussi ? : '+ p);});

          //on récupère une order au hasard pour l'instant
          order = orders[0];
          product = order['line_items'][0];

          //console.dir(product);

      		  // CONTACT ALIEXPRESS API
      		 var orderJSON = {};

           //CREDIT_CARD
           u_db_query.get_credit_card('krist-fit', function(result){

            orderJSON['credit_card']={};
            orderJSON['credit_card']['shop_id']=result['shop_id'];
            orderJSON['credit_card']['number']=result['number'];    
            orderJSON['credit_card']['first_name']=result['first_name'];
            orderJSON['credit_card']['family_name']=result['family_name'];
            orderJSON['credit_card']['crypto']=result['crypto'];
            orderJSON['credit_card']['expiration_date']=result['expiration_date'];
              

          });


           //VARIANTES
           variant_id = product['variant_id'];
          request.get('https://krist-fit.myshopify.com/admin/api/2019-04/variants/'+ variant_id +'.json', { headers: head , json : true})
          .then((result) => 
          {

            console.log(result['variant']['option1']);


          });


      		orderJSON['shopify_order_id'] = "123456";

      		orderJSON['product'] = {};
      		orderJSON['product']['product_id'] = orders[0]['line_items'][0]['id'];
      		orderJSON['product']['product_color'] = "V White";
      		orderJSON['product']['product_size'] = "M";

      		orderJSON['shipping_address'] ={};
      		orderJSON['shipping_address']['name'] = "name";
      		orderJSON['shipping_address']['street'] = "street";
      		orderJSON['shipping_address']['city'] = "city";
      		orderJSON['shipping_address']['zip'] = "zip";
      		orderJSON['shipping_address']['province'] = "province";
      		orderJSON['shipping_address']['mobile'] = "mobile";
      		      
      		orderJSON['aliexpress_account'] ={};
      		orderJSON['aliexpress_account']['aliexpress_id'] = aliexpress_id;
      		orderJSON['aliexpress_account']['aliexpress_pwd'] = aliexpress_pwd;


      		//orderProduct(orderJSON);

           


		 };}); 
      
	});

	}
	catch(error){
		console.dir(error);
	}
	console.log('date : '+date.getHours()+'H '+date.getMinutes()+'mins et '+date.getSeconds()+'Secs');
});

*/

/*
var jUpdateProducts = schedule.scheduleJob('* /5 * * * * *', function(){
 

	var date = new Date();
			
	try{
	request.get('https://krist-fit.myshopify.com/admin/products.json', { headers: head , json : true})
	.then((json_shopify) => 
	{
		json_shopify = json_shopify['products']
		u_db_query.get_products(config.SHOPIFY_ID, function(json_db){
		
		new_products = [];
		

		json_shopify.forEach(function(p1) {
			json_db.forEach(function(p2) {
          		if(p1['product_id'] == p2['shopify_product_id']){
          			new_products.push(p1);
          		}
   		 	}); 
   		 });
    console.dir(new_products);
		});

		
      
	});

	}
	catch(error){
		console.dir(error);
	}
	console.log('date : '+date.getHours()+'H '+date.getMinutes()+'mins et '+date.getSeconds()+'Secs');
});

*/



/**
 * Insert a product in the db
 *@name /api/insertProduct
*@property products.shopify_product_id    - {string}   
*@property products.shop_id    - {string}  -
*@property products.aliexpress_product_id    - {string}  -
*@property products.sell_price    - {real}  - 
*@property products.buy_price    - {real}  -
*@property products.stock    - {boolean}  - 
*@property products.variant    - {json}  -
 */

app.get('/api/insertProduct', cors(), (req,res)=>{
 
  json_product = [];
  json_product['shopify_product_id'] =  typeof req.query.shopify_product_id == 'undefined' ? '' : req.query.shopify_product_id ;
  json_product['shop_id'] = req.query.shop_id;
  json_product['aliexpress_product_id'] = req.query.aliexpress_product_id;
  json_product['sell_price'] = req.query.sell_rpice;
  json_product['buy_price'] = req.query.buy_price;
  json_product['stock'] = req.query.stock;
  json_product['variant'] = req.query.variant;
  console.log(json_product);
  u_db_query.insert_products(json_product,(res2)=>{

    res.send(res2);

  });


})
/**
 * Update variant from products from shopify in the db
*@name /api/update_variant
*@property products.shopify_product_id    - {string}   
*@property products.variant    - {string}   

 */


app.post('/api/update_variant', cors(), (req,res)=>{
//update variant in DB
  console.log(req.body[0]);

  console.log(req.body[1]);
  u_db_query.update_variant(req.body);
  res.end('OK');
})



/**
 * Insert new products from shopify in the db
 *@name /api/insertNewProducts
*@return success    - {boolean}   


 */


app.get('/api/insertNewProducts', cors(), (req,res)=>{

 try{
  request.get('https://krist-fit.myshopify.com/admin/products.json', { headers: head , json : true})
  .then((json_shopify) => 
  {
    json_shopify = json_shopify['products']
    u_db_query.get_new_products(json_shopify, function(json_db){
    
    new_products = [];

    json_shopify.forEach(function(p1) {

              if((json_db.indexOf(p1['id'])>=0)){
                  
                  json_product = [];
                  json_product['shopify_product_id'] =  typeof p1['id'] == 'undefined' ? '' : p1['id'] ;
                  json_product['shop_id'] = 'krist-fit';
                  json_product['aliexpress_product_id'] = '';
                  json_product['sell_price'] = p1['variants'][0]['price'];
                  json_product['buy_price'] = '';
                  json_product['name'] = p1['title'];
                  var variants = {} // empty Object
                  
                  p1['options'].forEach(function(p3) {
                    var key = p3.name;
                    variants[key] = [];
                    p3['values'].forEach(function(p4) {
                    var data = {
                    'shopify': p4,
                    'aliexpress': ''
                  }
                  variants[key].push(data);

                  });
                  });
                  json_product['stock'] = '';
                  json_product['variant'] = JSON.stringify(Object.values(variants));
                 
                             
                  u_db_query.insert_products(json_product,(res2)=>{

                      res.send(res2);

                    }); }
        
       });
    console.dir(new_products);
    });
});
 

  }
  catch(error){
    console.dir(error);
  }

})


/**
 * Return all orders for a given shopify id
 *@name /api/orders
*@property shopify_id    - {string}   
 */
app.get('/api/orders/:shop_id', cors(), (req, res) => {
//renvoie la liste des commandes pour une boutique shopify

shopify_id = typeof req.query.shopify_id !== 'undefined'  ? req.query.shopify_id : config.SHOPIFY_ID;

u_db_query.get_orders(req.params.shop_id, function(result){

	res.send(result);
});


});



/**
 * Return product info
 *@name /api/productInfo
*@property shopify_id    - {string}   
*@property shopify_product_id    - {string}   
*@return {json} product
 */
app.get('/api/productInfo', cors(), (req, res) => {
//renvoie la liste des produits pour une boutique shopify

shop_id = typeof req.query.shop_id !== 'undefined'  ? req.query.shopify_id : config.SHOPIFY_ID;
shopify_product_id = typeof req.query.shopify_product_id !== 'undefined'  ? req.query.shopify_id : '2018668707926';

u_db_query.get_product(shop_id, shopify_product_id, function(result){


	res.send( result);

});

});

/**
 * Return all products for a given shopify id
 *@name /api/products
*@property shopify_id    - {string}   
 */
app.get('/api/products/:id', cors(), (req, res) => {
//renvoie la liste des produits pour une boutique shopify

shopify_id = typeof req.query.shopify_id !== 'undefined'  ? req.query.shopify_id : config.SHOPIFY_ID;

u_db_query.get_products(req.params.id, function(result){

  res.send(result);
});

});


/**
 * Return all products for a given shopify id
 *@name /api/update_product
*@property query    - {string}   
 */
app.get('/api/update_product', cors(), (req, res) => {
	//update aliexpress id in DB
	console.log(req.query);
	u_db_query.update_product(req.query);
	res.end('OK');
});


/**
Route login :
Return info login and password of db
*/
app.get('/login', cors(), (req, res) => {
//renvoie la liste des comptes pour une boutique shopify

console.log("bien reçu");

u_db_query.get_accounts(function(result){res.send(result);} );



});

/*
Route insertion des comptes
Prend en paramètre un tableau stringify
*/

app.get('/insert_account/:product', cors(), (req, res) => {
//renvoie la liste des comptes pour une boutique shopify

//json_account = [];
 // json_account.push( req.params.product.split(','));
//var result = req.params.product
// envoie de la string 
u_db_query.insert_account(req.params.product,function(result){res.send(result);} );

});
/**
Return all info for one user except password
*/
app.get('/profile/:id', cors(), (req, res) => {
//renvoie la liste des infos pour une boutique shopify

shopify_id = typeof req.query.shopify_id !== 'undefined'  ? req.query.shopify_id : config.SHOPIFY_ID;
//res.send("Liste :")
console.log("la query:",req.query.shopify_id)
console.log("la config",config.SHOPIFY_ID)
console.log("le post :",{id :req.params.id})
// req.params.id permet de prendre la donnée sur URL de manière dynamique 
// methode pour utiliser le cookie de connexion pour mettre dans l'axios.get 
u_db_query.get_profile(req.params.id, function(result){ 

  res.send(result);
});
});

/*

Retourne les résultats avec le shop_id correspondant sur les routines de l'utilisateurs
*/
app.get('/campagne/:id', cors(), (req, res) => {

//console.log("arrete de spam",req.params.id)
// req.params.id permet de prendre la donnée sur URL de manière dynamique 
// methode pour utiliser le cookie de connexion pour mettre dans l'axios.get 
u_db_query.get_routine_nbAction(req.params.id, function(result){ 

  res.send(result);
});
});
app.get('/botAction/:id', cors(), (req, res) => {

console.log("arrete de spam",req.params.id)
// req.params.id permet de prendre la donnée sur URL de manière dynamique 
// methode pour utiliser le cookie de connexion pour mettre dans l'axios.get 
u_db_query.get_routineaction(req.params.id, function(result){ 

  res.send(result);
});
});


app.get('/test', (req, res) => {// repasser get-> post et suppr res.end
  var orderId=req.body.orderId; //donnée du flow
  orderId=898214821974;
  console.log('Order'+orderId);
  request.get('https://krist-fit.myshopify.com//admin/orders/'+orderId+'.json', { headers: head , json : true})
.then((response) => {
 var product = response.order.line_items[0].id;// retrieve on databas
  console.log(response.order);
 var variant=response.order.line_items[0].variant_title
  var customerAddress='Paris'//response.order.customer.default_address.address1
  var data= JSON.stringify({
  shopify_order_id:orderId,
  product_id: product,
  aliexpress_account:'pierre.etienne.horreau@gmail.com',
  shipping_address:customerAddress
});
var caca=JSON.parse('{ "shopify_order_id":'+orderId+',   	"product" : 	{ 		"product_id" : '+product+', 		"product_size" : "M", 		"product_color" : '+variant+' 	}      }');
res.end(JSON.stringify(caca));
request({
        url: "https://0e1d1436.ngrok.io/OrderProduct",
        json: true,
		method: "POST",
        headers: {
        "content-type": "application/json",
        },
		json: caca,
    }, function (error, response, body) {
        if (!error && response.statusCode === 200) {
            console.log(body)
        }
        else {

            console.log("error: " + error)
            console.log("response.statusCode: " + response.statusCode)
            console.log("response.statusText: " + response.statusText)
        }
    });
});
});















app.listen(3000, () => {
  console.log('Example app listening on port 3000!');
});

app.get('/shopify', (req, res) => {
  const shop = req.query.shop;
  if (shop) {
    const state = nonce();
    const redirectUri = forwardingAddress + '/shopify/callback';
    const installUrl = 'https://' + shop +
      '/admin/oauth/authorize?client_id=' + apiKey +
      '&scope=' + scopes +
      '&state=' + state +
      '&redirect_uri=' + redirectUri;

    res.cookie('state', state);
    res.redirect(installUrl);
  } else {
    return res.status(400).send('Missing shop parameter. Please add ?shop=your-development-shop.myshopify.com to your request');
  }
  
  
  
});



app.get('/shopify/callback', (req, res) => {
  const { shop, hmac, code, state } = req.query;
  const stateCookie = cookie.parse(req.headers.cookie).state;

  if (state !== stateCookie) {
    return res.status(403).send('Request origin cannot be verified');
  }

  if (shop && hmac && code) {
    // DONE: Validate request is from Shopify
    const map = Object.assign({}, req.query);
    delete map['signature'];
    delete map['hmac'];
    const message = querystring.stringify(map);
    const providedHmac = Buffer.from(hmac, 'utf-8');
    const generatedHash = Buffer.from(
      crypto
        .createHmac('sha256', apiSecret)
        .update(message)
        .digest('hex'),
        'utf-8'
      );
    let hashEquals = false;

    try {
      hashEquals = crypto.timingSafeEqual(generatedHash, providedHmac)
    } catch (e) {
      hashEquals = false;
    };

    if (!hashEquals) {
      return res.status(400).send('HMAC validation failed');
    }

    // DONE: Exchange temporary code for a permanent access token
    const accessTokenRequestUrl = 'https://' + shop + '/admin/oauth/access_token';
    const accessTokenPayload = {
      client_id: apiKey,
      client_secret: apiSecret,
      code,
    };

    request.post(accessTokenRequestUrl, { json: accessTokenPayload })
    .then((accessTokenResponse) => {
      const accessToken = accessTokenResponse.access_token;

     const shopRequestUrl = 'https://' + shop + '/admin/shop.json';
const shopRequestHeaders = {
  'X-Shopify-Access-Token': config.SHOPIFY_ACCESS_TOKEN,
};
head=shopRequestHeaders;
console.log(head);
request.get(shopRequestUrl, { headers: shopRequestHeaders })
.then((shopResponse) => {
  res.end(shopResponse);
})
.catch((error) => {
  res.status(error.statusCode).send(error.error.error_description);
});
      // TODO
      // Use access token to make API call to 'shop' endpoint
    })
    .catch((error) => {
      res.status(error.statusCode).send(error.error.error_description);
    });

  } else {
    res.status(400).send('Required parameters missing');
  }
});