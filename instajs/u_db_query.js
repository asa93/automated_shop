
/**
 * @file contain all functions that interact with main DB
 * @author Slimane & Pierre
 */


var config = require('./config.json');
config = config.DB_DEV;

var sql = require('mssql');


exports.insertAnalytics = function(analytics, callback){
 

    var queryToInsert = "";
    analytics.forEach(function(ele){
    queryToInsert  = queryToInsert + "('" + ele.date + "','" +  ele.username + "','" + ele.followers + "','"+ ele.followings + "','"+ ele.posts + "','" + ele.totalLikes +  "','" + ele.totalComments  + "','" + ele.averageLikes + "','" + ele.averageComments + "','" + ele.users + "'),";

    });

    queryToInsert = queryToInsert.substring(0, queryToInsert.length-1);

    var Result=[];
    var dbConn = new sql.ConnectionPool(config);
    //5.
    dbConn.connect().then(function () {

    var requests = new sql.Request(dbConn);


    requests.query("INSERT INTO instanalytic (date,username,followers,followings,posts,totalLikes,totalComments,averageLikes,averageComments, users) VALUES "+ queryToInsert +"").then(function (recordSet) {



    dbConn.close();
    sql.close();

    return callback('win');

    }).catch(function (err) {
    //8.
    console.log(err);
    dbConn.close();
    sql.close();
    });
    }).catch(function (err) {
    //9.
    console.log(err);
    });
}



exports.insertRoutine = function(routine, callback){

    var parameters = [
    { name: 'shop_id', sqltype: sql.NVarChar, value: routine.shopId},
    { name: 'routine_id', sqltype: sql.NVarChar, value: routine.routineId},
    { name: 'routine_name', sqltype: sql.NVarChar, value: routine.routineName},
    { name: 'start_date', sqltype: sql.NVarChar, value: routine.startTime}  
];


    var Result=[];
    var dbConn = new sql.ConnectionPool(config);
    //5.
    dbConn.connect().then(function () {

    var requests = new sql.Request(dbConn);

    parameters.forEach(function(p) {
        requests.input(p.name, p.sqltype, p.value);
    });

    requests.query("INSERT INTO routine (shop_id,routine_id,routine_name,start_date) VALUES(@shop_id,@routine_id,@routine_name,@start_date)").then(function (recordSet) {


    dbConn.close();
    sql.close();

    return callback('win');

    }).catch(function (err) {
    //8.
    console.log(err);
    dbConn.close();
    sql.close();
    });
    }).catch(function (err) {
    //9.
    console.log(err);
    });
}

exports.insertActions = function(actionList, callback){


         

        queryToInsert = "";
      actionList.forEach(function(ele){
        queryToInsert  = queryToInsert + "('" + ele.date + "','" +  ele.actionType + "','" + ele.objet + "','"+ ele.routine_id + "','"+ ele.shop_id + "','"+ ele.account +"'),";

      });

      queryToInsert = queryToInsert.substring(0, queryToInsert.length-1);


    var Result=[];
    var dbConn = new sql.ConnectionPool(config);
    //5.
    dbConn.connect().then(function () {
    //6.

    var requests = new sql.Request(dbConn);
    //7.

    requests.query("INSERT INTO botAction (date,action_type,object,routine_id,shop_id,account) VALUES "+queryToInsert+"").then(function (recordSet) {


    dbConn.close();
    sql.close();

    return callback('win');

    }).catch(function (err) {
    //8.
    console.log(err);
    dbConn.close();
    sql.close();
    });
    }).catch(function (err) {
    //9.
    console.log(err);
    });
}