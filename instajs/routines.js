const actions = require('./actions');
var schedule = require('node-schedule');
const Fusia = require("./modules/Fusia"); 

const  {Builder, By, logging, until, Key} = require('selenium-webdriver');
const webdriver = require('selenium-webdriver');
const chrome = require('selenium-webdriver/chrome');

const uuidv1 = require('uuid/v1');
const axios = require('axios');

var config = require('./config.json');

let o = new chrome.Options();
// o.addArguments('start-fullscreen');
o.addArguments('disable-infobars');

if(config.headless == "true")
o.addArguments('headless'); // running test on visual chrome browser

o.setUserPreferences({ credential_enable_service: false });



const sleep = (milliseconds) => {
  return new Promise(resolve => setTimeout(resolve, milliseconds))
}

formatDate = function(dateObj,format)
{
    var monthNames = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
    var curr_date = dateObj.getDate();
    var curr_month = dateObj.getMonth();
    curr_month = curr_month + 1;
    var curr_year = dateObj.getFullYear();
    var curr_min = dateObj.getMinutes();
    var curr_hr= dateObj.getHours();
    var curr_sc= dateObj.getSeconds();
    if(curr_month.toString().length == 1)
    curr_month = '0' + curr_month;      
    if(curr_date.toString().length == 1)
    curr_date = '0' + curr_date;
    if(curr_hr.toString().length == 1)
    curr_hr = '0' + curr_hr;
    if(curr_min.toString().length == 1)
    curr_min = '0' + curr_min;

    if(format ==1)//dd-mm-yyyy
    {
        return curr_date + "-"+curr_month+ "-"+curr_year;       
    }
    else if(format ==2)//yyyy-mm-dd
    {
        return curr_year + "-"+curr_month+ "-"+curr_date;       
    }
    else if(format ==3)//dd/mm/yyyy
    {
        return curr_date + "/"+curr_month+ "/"+curr_year;       
    }
    else if(format ==4)// MM/dd/yyyy HH:mm:ss
    {
        return curr_month+"/"+curr_date +"/"+curr_year+ " "+curr_hr+":"+curr_min+":"+curr_sc;       
    }
}

class Routine{
    //'BASIC MODEL OF A ROUTINE'
    constructor(){

        this.shop_id;
        this.maxActions = null;

        this.actionList=new Array(); //will list all the actions implemented by the routine
        this.nbActionsDone = 0;  //count of actions done by the routine so far

        this.errors = new Array(); //will list all the errors/exceptions encountered when processing routine
        
        this.startTime = null;  //starting time of the routine
        this.endTime = null; //actual ending time of the routine
        this.allocatedTime = null; //allocated time to process the routine. Note: allocatedTime pas toujours égal à endTime - startTime 
        
        this.account = null;
        
        this.routineName = 'INSERT ROUTINE NAME';
        this.routineDesc = 'INSERT DESCRIPTION';
        
        this.routineId = null;

        this.killRoutine = false; //when set to true, will end routine and launch this.stop();

        this.lastDayRunning = null;

        this.driver = null;

        this.ROUTINE_STATE = { DEAD : "DEAD", RUNNING : "RUNNING", FROZEN : "FROZEN", NEW : "NEW"};

        this.state = this.ROUTINE_STATE.NEW;

        this.routineId = uuidv1();



	}
	start(){
        //'Start the routine'
        this.startTime = formatDate(new Date,4);
        console.log('[CALL] starting routine at ' + this.startTime);

        this.lastDayRunning = formatDate(new Date,3);

        this.state = this.ROUTINE_STATE.RUNNING;

          let driver1 = new webdriver.Builder()
    .withCapabilities({'browserName': 'chrome','name':'Chrome Test','tz':'America/Los_Angeles','build':'Chrome Build','idleTimeout':'60','platform':'WINDOWS'})
      .setChromeOptions(o)
      .forBrowser('chrome')
      .build();

       this.driver = driver1;


	}
	
	stop(){
        this.killRoutine = true;
        this.state = this.ROUTINE_STATE.DEAD;
        this.driver.quit();
        this.stopTime = formatDate(new Date,4);
        console.log('stopping routine at ' + this.stopTime);

        //************insert in DB
        
	}

    freeze(){
        try{
            this.state = this.ROUTINE_STATE.FROZEN;
            this.driver.quit();
        }catch(e){console.log("[ERREUR] Ne peut pas freezer la routine ", this.routineId," : ", e) }
    
}
}

class Account {
  constructor(username,password) {
        this.username = username;
        this.password = password;
       
  }
}      

class LikeFollowing extends Routine{

  constructor(params) {
		super();

        this.account = params.account;
        this.routineDesc = "Like 3 posts of each following";
        this.routineName = "Like Following Routine";
        this.maxActions = params.maxActions;
        this.shopId = params.shop_id;
        this.driver = null;
  }
		
		
		async start() {

        super.start();

        await actions.login(this.driver, this.account.username, this.account.password);

		var usernames =new Array();
        usernames = Object.values(await actions.getFollowingUsernames(this.driver, this.account.username));
        var BreakException = {};
		
		var posts;
		try {
        for (var i = 0; i < usernames.length && this.nbActionsDone<this.maxActions && this.state == this.ROUTINE_STATE.RUNNING; i++){
			
			var username=usernames[i];
			//console.log(this.actionList.length);
			//console.log(this.maxActions);
            
            await this.driver.get("https://www.instagram.com/"+username+"/");
            
			posts = await actions.getPosts(this.driver);

            var likeCount = 0;
			var j;
			var LikeOver=false;

            for (j = 0; j < posts.length && LikeOver==false && this.nbActionsDone<this.maxActions && this.state == this.ROUTINE_STATE.RUNNING; j++){
            //########## plus petite boucle d'actions

                if(this.killRoutine == true){
                    console.log("closing routine");
                    this.stop();
                    return
                }

				var p=posts[j];
                //print(p)
                await p.click();

                await sleep(2000)

				var res = await actions.likePost(this.driver);
                
                if(res.actionType!="Already liked"){

					//console.log(this.routineId);
					res.routine_id = this.routineId;
					//console.log('actions : '+res.routine_id);
					res.shop_id = this.shopId;
					res.account=this.account.username;
					res.objet=username;
					await this.actionList.push(res);

                    this.nbActionsDone = this.nbActionsDone + 1;

					//console.log('Actionlist: ' + this.actionList);
                    likeCount+=1
				}
                if(likeCount==3)LikeOver=true;

                await this.driver.executeScript("window.history.go(-1)");

                //time.sleep(0.3)
			}
			
			
		}
			}catch (e) {
			if (e !== BreakException) console.log('Catched exception : ' + e);
				console.log('End Of Actions');
				}

                this.freeze();
}
  
}

class PostLinkRoutine1 extends Routine{
  constructor(params) {
        super();
        this.account = params.account;
        this.routineDesc = "Post link on posts given some hashtags";
        this.routineName = "PostLinkRoutine1";
        this.maxActions = params.maxActions;
        this.shopId = params.shop_id;

        this.hashtags = params.hashtags;

        
  }

        async start() {
        super.start();
        var self = this;

        await sleep(1);

        await actions.login(this.driver, this.account.username, this.account.password);

        await sleep(3000);

        var maxActionByHashtag = Math.ceil(self.maxActions / this.hashtags.length)  ;

        
        for(var j = 0;  self.nbActionsDone < self.maxActions  ; j++){


            //console.log(self.actionList);

            var hashtag = this.hashtags[j];
            var nbActionsByHashtag = 0;

            //console.log("nbactions", maxActionByHashtag, ' for ', hashtag);
             await this.driver.get("https://www.instagram.com/explore/tags/" + hashtag);

             await sleep(3000);
             var posts = await actions.getPosts(this.driver);
             var postLinks = [];
             posts.forEach( (p)=>{
                postLinks.push(p.getAttribute('href'));

             });


            for(var i=0; i<postLinks.length && nbActionsByHashtag < maxActionByHashtag && this.nbActionsDone<this.maxActions ; i++){
                //plus petite boucle
                if(self.killRoutine == true){
                    self.driver.close();
                    self.stop();
                    return
                }


                var p = postLinks[i];

                await this.driver.get(p);

                //await post.click();

                await sleep(4000);

                try{

                    var res = await actions.getLikes(this.driver);


                    try{
                        if(res > 100){
                            var action = await actions.comment(self.driver,'Great post keep it going');
                            action.routine_id = self.routineId;
                            action.account = self.account.username;
                            self.actionList.push(action);
                            self.nbActionsDone = self.nbActionsDone + 1;
                            nbActionsByHashtag = nbActionsByHashtag + 1;
                            await sleep(1000);
                } 

                    }catch(e){
                        console.log("[ERROR] failed to comment.")
                    }


                }catch(e){
                    console.log("[ERROR] failed to get likes.")
                }
                
            

            }
           
        }


        this.freeze();

        
}
  
}


class FollowFollowers extends Routine{

  constructor(params) {
        super();

        this.account = params.account;
        this.routineDesc = "Follow followers from a user";
        this.routineName = "Follow followers Routine";
        this.maxActions = params.maxActions;
        this.shopId = params.shop_id;
        this.driver = null;
        this.user=params.user;

        //#### SPECIFIC PARAMS

        this.personsToFollow = params.personsToFollow;
  }
        
        
        async start() { 

        let Instagram = require('./modules/instagram');

            super.start();
            this.driver.quit();
            //LOGIN
        let fusia = await new Fusia({ username: this.account.username, password: this.account.password, debug: true });
            try {
        Instagram = new Instagram();
        let a=0;
        await fusia.login();
        console.log(fusia.userId);
        let user=await Instagram.getUserDataByUsername(this.user);
        let UserId=user.graphql.user.id;
        let end_cursor='';
        let follower = await fusia.userFollowers({ userId: UserId, count:50});
        let result='';
        let followers=[].concat(follower.edge_followed_by.edges);
        //Get Users followers Loop
            do{
                a=a+1;
                console.log(a);
                end_cursor=follower.edge_followed_by.page_info.end_cursor;
                follower = await fusia.userFollowers({ userId: UserId, count:50,cursor:end_cursor});
                followers=followers.concat(follower.edge_followed_by.edges);
                console.log('followers : ' +followers.length);
                //folllow users
                for(var i=0;i<followers.length && this.nbActionsDone<this.maxActions;i++)
                    {
                        console.log(followers[i]);
                        if(!(followers[i].node.followed_by_viewer ||  followers[i].node.requested_by_viewer) ){
                             result=await actions.follow(followers[i],this,fusia);
                             console.log(result);

                             await sleep(10000);
                             console.log('actions Done :'+this.nbActionsDone);
                         }

                    }
                await sleep(60000);
            }while(follower.edge_followed_by.page_info.has_next_page && this.nbActionsDone<this.maxActions);
        
        
        await fusia.logout();
        this.freeze();
    } catch (error) {
        console.log('Erreur de follow :'+error);
        await fusia.logout();
         this.freeze();
    }
}
  
}




module.exports.FollowFollowers = FollowFollowers;
module.exports.Account=Account;
module.exports.Routine=Routine;
module.exports.LikeFollowing=LikeFollowing;
module.exports.PostLinkRoutine1 = PostLinkRoutine1;