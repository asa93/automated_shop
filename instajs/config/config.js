    
var in_client_id = 'e534c066e96849ed889d58d4a358bb9f',
		in_client_secret = '99df0cb5af924dcf98905bf1c0335630',
		in_redirect_uri = 'http://localhost:3001/auth',
		in_auth_url = 'https://api.instagram.com/oauth/authorize/?client_id=' + in_client_id + '&redirect_uri=' + in_redirect_uri + '&response_type=code'
module.exports = {

	port: process.env.PORT || 3001
	,
	LinkqueryHash:'https://www.instagram.com/static/bundles/ecss/Consumer.js/a0d334f13d43.js'
	,
	instagram: {
		client_id: in_client_id,
		client_secret: in_client_secret,
		auth_url: in_auth_url,
		redirect_uri: in_redirect_uri
	}

};