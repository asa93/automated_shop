const express = require('express');
const app = express();
const request = require('request-promise');
var schedule = require('node-schedule');
var config = require('./config/config');


let Instagram = require('./modules/instagram');

const  {Builder, By, logging, until, Key} = require('selenium-webdriver');
const webdriver = require('selenium-webdriver');
const chrome = require('selenium-webdriver/chrome');
const actions = require('./actions');
const routines = require('./routines');
const fetch = require('node-fetch');
const u_db_query = require('./u_db_query');
//instalitycs
const { getQuickStats } = require('./instalitycs/Instalitycs');
const { getUrlByShortcode } = require('./instalitycs/instapro');
const axios = require('axios');
const uuidv1 = require('uuid/v1');
var routineList=[];

var config2 = require('./config.json');

const cors = require('cors')

const sleep = (milliseconds) => {
  return new Promise(resolve => setTimeout(resolve, milliseconds))
}
let o = new chrome.Options();
// o.addArguments('start-fullscreen');
o.addArguments('disable-infobars');
//o.addArguments('headless'); // running test on visual chrome browser
o.setUserPreferences({ credential_enable_service: false });



//socialytics data
app.get('/socialitycs/:id', cors(), (req, res) => {

  var desiredNumberOfTopPosts = 6;
  getQuickStats(req.params.id, desiredNumberOfTopPosts).then(stats => {
      res.end(JSON.stringify(stats));
  	//console.log('called root: '+req.params.id);
    });

  });
//media URL data

app.get('/media/:shortcode', cors(), (req, res) => {

  var shortcode=req.params.shortcode;
  getUrlByShortcode(shortcode).then(stats => {
  	
  	console.log(stats);
      res.end(JSON.stringify(stats).substring(1,stats.length-1));
  });


  });
  

app.get('/test/', cors(), (req, res) => {

  res.end(JSON.stringify(req.query));
  });


// __________________________________________ROUTE FOR ALL ROUTINE CALL
app.get('/start_routine', cors(),(req, res) => {
  //example
  //http://localhost:3001/start_routine?username=luxurylife__paris&password=alix.shop_303&shop_id=krist_fit&routine_name=LikeFollowing&maxActions=100
  
try{

    var acc = new routines.Account(req.query.username,req.query.password);

    if(req.query.username == undefined || req.query.password == undefined){   
      res.send("Please pass on credentials !") ;
      throw "Please pass on instagram credentials ass parameters!"; }


  switch(req.query.routine_name){
    case "PostLinkRoutine1":
        //http://localhost:3001/start_routine?username=luxurylife__paris&password=alix.shop_303&shop_id=krist_fit&routine_name=PostLinkRoutine1&maxActions=5&hashtags=fitgirl,fitness
        var params = {
        account : acc,
        shop_id : req.query.shop_id,
        hashtags : req.query.hashtags != undefined ? req.query.hashtags.split(",") : undefined ,
        maxActions : req.query.maxActions, };

        if(checkParams(params) != true)
          throw "Not enough parameters";
        
      var routine1 = new routines.PostLinkRoutine1(params);

    break;

	case "LikeFollowing":
        var params = {
        account : acc,
        shop_id : req.query.shop_id,
        maxActions : req.query.maxActions};
        if(checkParams(params) != true)
          throw "Not enough parameters";

      var routine1 = new routines.LikeFollowing(params);
    break;
 case "FollowFollowers":
     //http://localhost:3001/start_routine?username=luxurylife__paris&password=alix.shop_303&shop_id=krist_fit&routine_name=FollowFollowers&maxActions=5&user=1booty1lingerie
      
        var params = {
        account : acc,
        shop_id : req.query.shop_id,
        maxActions : req.query.maxActions,
        user : req.query.user};
        if(checkParams(params) != true)
          throw "Not enough parameters";

      var routine1 = new routines.FollowFollowers(params);
    break;

    default : 
      res.send("No routine found for given name :(");
      driver1.close();
      throw "No routine found for given name :( ";

  }

  routineList.push(routine1);

  try{ 
	routine1.start();
    res.send("Routine launched with id " + routine1.routineId); 

      try{ u_db_query.insertRoutine(routine1, (res)=>{ }) } catch(e){console.log("couldn't insert routine in DB", e)}

    } catch(e) { console.log("[ERROR] failed to launch routine: ", e); res.send("failed to launch routine : " + e)}

}catch(error){console.log("[ERROR] failed to launch routine :", error); res.send("failed to launch routine : " + error)}

});


app.listen(3001, () => {
  console.log('Example app listening on port 3001!');
});


//____________________________KILL ROUTINE

app.get('/kill_routine', (req, res) => {

  //example
  //localhost:3001/start_routine/?routine_id=
  routine_id = req.query.routine_id;

  routineList.forEach((routine)=>{

    if(routine.routineId == routine_id){
      routine.killRoutine = true;
      var index = routineList.indexOf(routine);
      routineList.splice(index,1);
      res.send("Killed routine " + routine_id);
    }

  });
  res.send("The given id doesn't correspond to any routine :(");

  });


//________________ROUTE

app.get('/getAnalytics', (req, res) => {

  //get instagram analytics every hour for the username and add it to the DB

  //localhost:3001/getAnalytics?username=1booty1lingerie&password=pe123456


  console.log("[CALL] /getAnalytics");

  if(checkParams(req.params) != true)
    res.send("Vous n'avez pas envoyé assez d'arguments");

  analyticsToGet.push({ "username" : req.query.username, "password" : req.query.password});

  res.send("Nous récupérerons vos statistiques instagram toutes les heures. Rafraîchissez régulièrement la page pour obtenir les dernières stats.");

  });
 

//####################################################
//#####################################################         [J  O   B   S]

// ___________________ MASTER ROUTINE JOB
var flushJob =  schedule.scheduleJob(config2.FLUSH_CRON, async function(){
  console.log("[FLUSHING JOB] ", formatDate(new Date, 4), " => ", routineList.length, " routines en cours.");

  

   for(var i=0; i < routineList.length ; i++){


    var routine = routineList[i];
     console.log(routine.routineId, "/", routine.state, " / NB ACTIONS DONE : ", routine.nbActionsDone, " / NB MAX ACTIONS :", routine.maxActions);

    if(routine.state  == routine.ROUTINE_STATE.DEAD){
      var index = routineList.indexOf(routine);
      routineList.splice(index,1);
    }

    if(routine.actionList.length > 0){

      //console.log("routine :", routine.routineId);
      //console.log("insertion de :"+ routine.actionList.length+  " actions dans la DB.");

      try{ await u_db_query.insertActions(routine.actionList, (result)=>{ })  }catch(error){console.log("[ERROR] Erreur insertion dans la base de donnée : ", error)} 

      routine.actionList = [];
  }

  }
  });  


var resetJob =  schedule.scheduleJob(config2.REINIT_CRON, async function(){
  //A minuit on réinitialise le compteur d'actions
  
  console.log("[RESET JOB] ",formatDate(new Date, 4));


  for(var i=0; i < routineList.length ; i++){
    var routine = routineList[i];
    if(routine.state != routine.ROUTINE_STATE.FROZEN)
      routine.freeze();
  }

  await sleep(5000);

    for(var i=0; i < routineList.length ; i++){    
      routine.nbActionsDone = 0;
      routineList[i].start();
      await sleep(10000)
  }

  });

// ____________________[ANALYTICS JOB]
var analyticsToGet = [];
var analyticsToPush = [] ;

var analyticsJob =  schedule.scheduleJob(config2.ANALYTICS_CRON, async function(){
  
  console.log("[ANALYTICS JOB] ", formatDate(new Date, 4), " => ", analyticsToGet.length, " comptes analysés.");

  try{

    analyticsToGet.forEach( (user)=>{


        var username = user.username;
        var password = user.password;


        const url = 'http://localhost:3001/socialitycs/' + username;
        axios.get(url)
        .then(async (data) => {
            var res = data.data;


            var user_data;



            var usernames1 = await actions.ListFollowerS(username,password);
            var usernames = [];

            for(let i = 0; i<usernames1.length ; i++)
              usernames.push(usernames1[i].text);

            //console.log("usernames", usernames);

            user_data = await actions.getUserDataByUsernames(usernames);

            console.log("user data", user_data);




            /*


            await usernames.forEach( async (username)=>{
                console.log(usernames.indexOf(username), " /info on ", username.text);
                let nbFollowers = "nbFollowers";
                
                try{
                    
                    //nbFollowers = user_data.graphql.user.edge_followed_by.count;

                    console.log(username.text, user_data);
                    await sleep(1000);
                  //user_data.graphql.user.edge_followed_by.count
                  await users.push({"username": username.text, "followers" : nbFollowers});
                }catch(e){nbFollowers = "N/A", await users.push({"username": username.text, "followers" : nbFollowers}); console.log("[ERREUR] ", e)}



            });

            //console.log("users",users); 
            


            analytic = {
                "date" : formatDate(new Date,4)
                
                ,"username" : res.username,
                "followers" : res.followers,
                "followings" : res.following,
                "posts" : res.posts,
                "totalLikes" : res.totalLikes,
                "totalComments": res.totalComments,
                "averageLikes": res.averageLikes,
                "averageComments": res.averageComments
                 ,"users" : Object.values(users).toString() 
 
            };

            //console.log(analytic);

            
            

           
            analyticsToPush.push(analytic);

            */

        })
        .catch(e => console.log("[ERREUR]",e));

    });

    if(analyticsToPush.length > 0){
      try{
        await u_db_query.insertAnalytics(analyticsToPush, (res)=>{
        //console.log("[ANALYTICS] insertion of ", analyticsToPush.length, " analytics in the DB.");
        analyticsToPush = [];
        });
      }catch(e){ console.log("[ERREUR] couldn't insert into DB.")}
    
    }

  }catch(e){console.log("[ERREUR] couldn't get analytics", e)}

  });

/*
 let driver1 = new webdriver.Builder()
.withCapabilities({'browserName': 'chrome','name':'Chrome Test','tz':'America/Los_Angeles','build':'Chrome Build','idleTimeout':'60','platform':'WINDOWS'})
 .setChromeOptions(o)
.forBrowser('chrome')
.build();
async function run() {
 
await actions.login(driver1,'1booty1lingerie','pe123456');
await actions.ListFollowings(driver1,'1booty1lingerie');

}
run();
*/

//test
/*
Instagram = new Instagram();

Instagram.getCsrfToken().then((csrf) =>
{
  Instagram.csrfToken = csrf;
}).then(() =>
{
  return Instagram.auth('1booty1lingerie','pe123456').then(sessionId =>
  {
    Instagram.sessionId = sessionId
	console.log(sessionId);
    return Instagram.getUserDataByUsername('jordy_yakoi').then((t) =>
    {
		console.log(t.graphql.user.id);
		var  date1 =Math.floor(Date.now() / 1000);;
		console.log(date1);
      return Instagram.getUserFollowingss(t.graphql.user.id).then((t) =>
      {
        console.log(t); // - instagram followers for user "username-for-get"
		var date2=Math.floor(Date.now() / 1000);
		var tmp = date2 - date1;
		console.log(tmp+' secondes');
      })
	  
    })

  })
}).catch(console.error);


*/



//######################################
//#######################################          O  T  H   E   R           F   U   N   C   T   I   O   N   S
formatDate = function(dateObj,format){
    var monthNames = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
    var curr_date = dateObj.getDate();
    var curr_month = dateObj.getMonth();
    curr_month = curr_month + 1;
    var curr_year = dateObj.getFullYear();
    var curr_min = dateObj.getMinutes();
    var curr_hr= dateObj.getHours();
    var curr_sc= dateObj.getSeconds();
    if(curr_month.toString().length == 1)
    curr_month = '0' + curr_month;      
    if(curr_date.toString().length == 1)
    curr_date = '0' + curr_date;
    if(curr_hr.toString().length == 1)
    curr_hr = '0' + curr_hr;
    if(curr_min.toString().length == 1)
    curr_min = '0' + curr_min;

    if(format ==1)//dd-mm-yyyy
    {
        return curr_date + "-"+curr_month+ "-"+curr_year;       
    }
    else if(format ==2)//yyyy-mm-dd
    {
        return curr_year + "-"+curr_month+ "-"+curr_date;       
    }
    else if(format ==3)//dd/mm/yyyy
    {
        return curr_date + "/"+curr_month+ "/"+curr_year;       
    }
    else if(format ==4)// MM/dd/yyyy HH:mm:ss
    {
        return curr_month+"/"+curr_date +"/"+curr_year+ " "+curr_hr+":"+curr_min+":"+curr_sc;       
    }
}


checkParams = function(params){

  var param;
  for(key in params){

    if(params[key] == undefined)
      return false;
  }

  return true;
  }