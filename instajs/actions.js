/*login
getFollowingUsernames
getPosts
likePost*/
const  {Builder, By, logging, until, Key} = require('selenium-webdriver');
const webdriver = require('selenium-webdriver');
const BOT_FOLLOW_PERSON = "bot follow person";
const PERSON_FOLLOW_BOT= "person follow bot";
const BOT_UNFOLLOW_PERSON = "bot unfollow person";
const PERSON_UNFOLLOW_BOT = "person unfollow bot";
const BOT_LIKE_PERSON = "bot like person";
const BOT_ALREADY_LIKED="Already liked";

const sleep = (milliseconds) => {
  return new Promise(resolve => setTimeout(resolve, milliseconds))
}

function formatDate(dateObj,format)
{
    var monthNames = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
    var curr_date = dateObj.getDate();
    var curr_month = dateObj.getMonth();
    curr_month = curr_month + 1;
    var curr_year = dateObj.getFullYear();
    var curr_min = dateObj.getMinutes();
    var curr_hr= dateObj.getHours();
    var curr_sc= dateObj.getSeconds();
    if(curr_month.toString().length == 1)
    curr_month = '0' + curr_month;      
    if(curr_date.toString().length == 1)
    curr_date = '0' + curr_date;
    if(curr_hr.toString().length == 1)
    curr_hr = '0' + curr_hr;
    if(curr_min.toString().length == 1)
    curr_min = '0' + curr_min;

    if(format ==1)//dd-mm-yyyy
    {
        return curr_date + "-"+curr_month+ "-"+curr_year;       
    }
    else if(format ==2)//yyyy-mm-dd
    {
        return curr_year + "-"+curr_month+ "-"+curr_date;       
    }
    else if(format ==3)//dd/mm/yyyy
    {
        return curr_date + "/"+curr_month+ "/"+curr_year;       
    }
    else if(format ==4)// MM/dd/yyyy HH:mm:ss
    {
        return curr_month+"/"+curr_date +"/"+curr_year+ " "+curr_hr+":"+curr_min+":"+curr_sc;       
    }
}
class BotAction{
    //"""Contains Everything About An Action in order to store it in DB"""
    
    constructor(actionType, date = formatDate(new Date,4),shop_id ="",objet="", account="", routine_id = ""){
        this.date = date;
        this.actionType = actionType;
        this.objet = objet;
        this.routine_id = routine_id;
        this.shop_id = shop_id;
        this.account = account;
	}
   
}
        
   
exports.login = async function(driver, username , password ){
    
	try{
	// Load page
    await driver.get("https://www.instagram.com/accounts/login/");
	await driver.findElement(By.css('html')); // WAIT UNTIL PAGE FULLY CHARGED
    // Login
    await driver.findElement(By.xpath("//div/input[@name='username']")).sendKeys(username);

	await driver.findElement(By.xpath("//div/input[@name='password']")).sendKeys(password);
    await driver.wait(until.elementIsNotSelected(driver.findElement(By.xpath("//div/input[@name='password']"))),5000);
    await driver.findElement(By.xpath("//form/div/button")).click();
	await sleep(2000); // WAIT UNTIL PAGE FULLY CHARGED
	return;
	}catch(error) {
		console.error(error);
	}
}

var accessFollowingList = async function(driver, username){
	await driver.findElement(By.css('html')); // WAIT UNTIL PAGE FULLY CHARGED
	await sleep(5000);
    driver.get("https://www.instagram.com/"+username);
	await driver.wait(until.elementsLocated(By.xpath("//section/ul/li/a")),5000);
    el = await driver.findElements(By.xpath("//section/ul/li/a"));
    el[1].click();
    
    return 1;
}
exports.getFollowingUsernames= async function(driver, username){
	await driver.findElement(By.css('html')); // WAIT UNTIL PAGE FULLY CHARGED
    await accessFollowingList(driver,username);
    var texte='';
    //time.sleep(1);
    
    followingList = [];

    scrollY = 500;
    
	
	await sleep(5000);
    //time.sleep(1);
    while (followingList.length < 115){
		await driver.executeScript("followersbox = document.querySelectorAll(' div[role=\"dialog\"] > div')[1];");
		await driver.executeScript("followersbox.scrollTo(0, "+scrollY+");");
		await sleep(500);
		await driver.executeScript("followersbox.scrollTo(250, "+500+");");
		
        followingList = await driver.findElements(By.xpath("//div/ul/div/li/div"));

        scrollY += 1000;
	}
    var usernames = new Array();
    var i;
    for (i = 0; i < followingList.length; i++){
		f=followingList[i];
		texte=await f.findElement(By.xpath("./div/div/div/a")).getText();
		usernames.push(texte);
	}	
        
    return usernames;
}

exports.getPosts= async function(driver){
	await driver.findElement(By.css('html')); // WAIT UNTIL PAGE FULLY CHARGED
    await driver.executeScript("window.scrollTo(0, 2000);")
    await sleep(200);
    var ele = await driver.findElements(By.xpath("//div/div/a"));
	var i;
	var posts=[];
    for (i = 0; i < ele.length; i++){
		texte = await ele[i].getAttribute('href');
		//console.log('texte : '+texte);
		if(await texte.includes('/p/')) posts.push(ele[i]);
    }
    return posts;
}
exports.likePost= async function(driver){
	await driver.findElement(By.css('html')); // WAIT UNTIL PAGE FULLY CHARGED
    ele = await driver.findElements(By.xpath("//section/span/button"));

    like_btn = ele[0];
    like_btn_info = await ele[0].findElement(By.xpath("./span"));
    var already_liked = await like_btn_info.getAttribute('class');
	already_liked=await already_liked.includes("red");

    if(!already_liked){
        await like_btn.click();
        return await new BotAction(BOT_LIKE_PERSON);
    }
    return new BotAction(BOT_ALREADY_LIKED);
}
exports.getLikes= async function(driver){

	
	await driver.findElement(By.css('html')); // WAIT UNTIL PAGE FULLY CHARGED
    //console.log("get likes");
    var res = await driver.findElement(By.xpath("//section/div/div/a/span")).getText();

    return parseInt(res);
}

exports.follow= async function(user,routine,fusia){

                        result=await fusia.follow({id:user.node.id,isFollow:true});
                            console.log(result);
                            var action = await new BotAction("Follow");
                            action.routine_id = routine.routineId;
                            action.objet=user.node.username;
                            action.account = routine.account.username;
                            routine.actionList.push(action);
                            routine.nbActionsDone = routine.nbActionsDone + 1;

                        
                        return result;
}


exports.getNumbers = async function(driver, username){
		

        await driver.get("https://www.instagram.com/"+username+"/")
		await driver.findElement(By.css('html')); // WAIT UNTIL PAGE FULLY CHARGED
       var ele = await driver.findElements(By.xpath("//ul/li/a"));


       var action = new BotAction("Analytics");
       var result = {};
       var nbFollowers = null;
       var nbFollowings = null;


       var ele = await driver.findElements(By.xpath("//ul/li/a"));

         for (var i = 0; i < ele.length; i++){
             var texte = await ele[i].getAttribute('href');
            if(await texte.includes('followers')) {
                nbFollowers =  await ele[i].getText();
                result['nbFollowers'] = nbFollowers;

            }
        }

         for (var i = 0; i < ele.length; i++){
             var texte = await ele[i].getAttribute('href');
            if(await texte.includes('following')) {
                nbFollowings =  await ele[i].getText();
                result['nbFollowings'] = nbFollowings;

            }
        }

        action.objet = JSON.stringify(result);
        return action;

}
exports.comment = async function(driver, text){

    
    try{

       await driver.findElement(By.xpath("//article/div/section/div/form/textarea")).click();
	   await driver.findElement(By.xpath("//article/div/section/div/form/textarea")).sendKeys(text);
	   
	   await driver.findElement(By.xpath("//article/div/section/div/form/textarea")).sendKeys(Key.ENTER);;

     var action = new BotAction("Comment");


     return action;

       //await textarea[0].click();
       //console.log(await textarea[0]);
    }catch(e){console.log(e)}

    
}


/*
exports.ListFollowings = async function(driver, username){
	await sleep(2000);
	await driver.findElement(By.css('html')); // WAIT UNTIL PAGE FULLY CHARGED
	await driver.get("https://www.instagram.com/accounts/access_tool/accounts_following_follow");
    await driver.findElement(By.css('html')); // WAIT UNTIL PAGE FULLY CHARGED
	
    var usernames = new Array();
	await sleep(2000);
	var texte=[];
    var followingList = [];
	var followingLists=[];
	do {
	await driver.findElement(By.xpath("//main/div/article/main/button")).click();
	await driver.wait(until.elementsLocated(By.xpath("//article/main/section/div")),5000);
	await sleep(100);
	followingLists=await driver.findElements(By.xpath("//article/main/section/div"));
	
		for(var j=0; j<followingLists.length;j++){
			followingList=await driver.findElement(By.xpath("//article/main/section/div"));
			//console.log(await followingList.getText());
			usernames.push(await followingList.getText());
			await driver.executeScript("var div=document.querySelectorAll('article > main > section > div')[0];div.parentNode.removeChild(div)");
			
		}
	console.log(usernames.length);
	}while(followingLists.length>0);
	console.log(usernames.length)
    //time.sleep(1);
    
    return usernames;
   
}


*/

exports.getUserDataByUsername = async function(username){
    let Instagram = require('./modules/instagram');
    Instagram = new Instagram();

    return Instagram.getCsrfToken().then((csrf) =>
    {
      Instagram.csrfToken = csrf;
    }).then(() =>
    {
        return Instagram.getUserDataByUsername(username);

    });
    
}

exports.getUserDataByUsernames = async function(usernames){
    console.log("getUserData");
    var result = [];
    let Instagram = require('./modules/instagram');
    Instagram = new Instagram();
 

        for(let i= 0; i<usernames.length; i++){
            let data = await Instagram.getUserDataByUsername(usernames[i])
            result.push(data);
            console.log(data.graphql.user.edge_follow.count);

        }
        

        return result;

    
}


exports.ListFollowerS = async function(username,password){

let Instagram = require('./modules/instagram');
	Instagram = new Instagram();

return Instagram.getCsrfToken().then((csrf) =>
{
  Instagram.csrfToken = csrf;
}).then(() =>
{
  return Instagram.auth(username,password).then(sessionId =>
  {
    Instagram.sessionId = sessionId
    //console.log(sessionId);
    return Instagram.getUserDataByUsername(username).then((t) =>
    {
        //console.log(t.graphql.user.id);
        var  date1 =Math.floor(Date.now() / 1000);;
        //console.log(date1);
      return Instagram.getUserFollowingss(t.graphql.user.id).then((t) =>
      {
        console.log("finished"); 
        return t;
        // - instagram followers for user "username-for-get"
        var date2=Math.floor(Date.now() / 1000);
        var tmp = date2 - date1;
        //console.log(tmp+' secondes');
      })
      
    })

  })
}).catch(console.error);


}
/*
exports.ListFollowings = async function(driver, username){
	await sleep(2000);
	await driver.findElement(By.css('html')); // WAIT UNTIL PAGE FULLY CHARGED
	await driver.get("https://www.instagram.com/accounts/access_tool/accounts_following_you?__a=1&cursor=");
    await driver.findElement(By.css('html')); // WAIT UNTIL PAGE FULLY CHARGED
	
    var usernames = new Array();
	await sleep(2000);
	var texte=[];
    var followingList = [];
	var followingLists=[];
	var a=0;
	try{
	do {
	a=a+1;
	await driver.wait(until.elementsLocated(By.xpath("//main/div/article/main/button")),5000);
	await driver.wait(until.elementIsEnabled(driver.findElement(By.xpath("//main/div/article/main/button"))),5000);
	await driver.findElement(By.xpath("//main/div/article/main/button")).click();
	
	console.log(a);
	}while(750>a);
	}catch(e){
	followingLists=await driver.findElements(By.xpath("//article/main/section/div"));
	
		for(var j=0; j<followingLists.length;j++){
			
			//console.log(await followingList.getText());
			usernames.push(await followingLists[j].getText());
			console.log(j);
			
		}
		console.log(usernames.length);
	}
	console.log(usernames.length)
    //time.sleep(1);
    
    return usernames;
   
}

exports.ListFollowerS = async function(username,password){
    	Instagram = new Instagram();

Instagram.getCsrfToken().then((csrf) =>
{
  Instagram.csrfToken = csrf;
}).then(() =>
{
  return Instagram.auth(username,password).then(sessionId =>
  {
    Instagram.sessionId = sessionId
    console.log(sessionId);
    return Instagram.getUserDataByUsername(username).then((t) =>
    {
        console.log(t.graphql.user.id);
        var  date1 =Math.floor(Date.now() / 1000);;
        console.log(date1);
      return Instagram.getUserFollowingss(t.graphql.user.id).then((t) =>
      {
        console.log(t); // - instagram followers for user "username-for-get"
        var date2=Math.floor(Date.now() / 1000);
        var tmp = date2 - date1;
        console.log(tmp+' secondes');
      })
      
    })

  })
}).catch(console.error);


}*/
module.exports.BotAction=BotAction;