''' @Slimane ASA'''
import time
#from instapyDB import *

BOTNAME = "un_deux_trois_bijoux"
PASSWORD = "mdp123456"

POST = "POST"
LIKE = "LIKE"


class Post:
    """Post element in Instagram"""
    
    def __init__(self,arg):
        self.url = arg
    
    def getComments(self,driver):
        if(driver.current_url != self.url) :
            driver.get(self.url)
            
        all_comments = driver.find_elements_by_xpath("//article/div/div/ul/ul/li")

        comments = []
        for x in all_comments:
            try :
                x.find_element_by_xpath(".//div/div/div/h3")
                comments.append(x)
            except :
                pass


        return  comments
    
    def likeComment(self, driver, comment):
        
        like_btn = comment.find_element_by_xpath("./div/span/button/span")
        like_btn.click()
        driver.execute_script("arguments[0].click()", like_btn)
        
    def comment(self, driver, text):
        try :
            driver.find_element_by_xpath("//article/div/section/div/form/textarea").send_keys(text)
            time.sleep(0.2)
            driver.find_element_by_xpath("//article/div/section/div/form/textarea").send_keys(Keys.ENTER)
        except:
            return "FAILED"
        
    def getLikes(self, driver):
        try :
            return driver.find_element_by_xpath("//div/section/div/div/button/span").text

        except :
            pass
        try:
            return driver.find_element_by_xpath("//section/div/div/a/span").text
        except :
            pass