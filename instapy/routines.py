import datetime
import time
import actions
from selenium import webdriver
from selenium.common.exceptions import TimeoutException, WebDriverException
class Routine(): 
    'BASIC MODEL OF A ROUTINE'
    def __init__(self):
        self.actionList=[] #will list all the actions implemented by the routine
        self.errors = [] #will list all the errors/exceptions encountered when processing routine
        
        self.startTime = None  #starting time of the routine
        self.endTime = None #actual ending time of the routine
        self.allocatedTime = None #allocated time to process the routine. Note: allocatedTime pas toujours égal à endTime - startTime 
        
        self.account = None
        
        self.routineName = 'INSERT ROUTINE NAME'
        self.routineDesc = 'INSERT DESCRIPTION'
        
        self.maxActions = None
        
    def start(self):
        'Start the routine'
        self.startTime = datetime.datetime.now()
        print('starting routine at ' + str(self.startTime) )
        
        #INSERT ROUTINE CODE
        
    def stop(self):
        'Stop routine'
        'Launch after routine stop'
        self.stopTime = datetime.datetime.now()
        print('stopping routine at ' + str(self.stopTime))
        
    def flush(self):
        'Insert the performed actions into the DB'
        'Insert the routine details into log'
    
        for action in self.actions:
            action.routineName = self.routineName
            if self.account is not None: 
                action.account = self.account.username
                
                
        #DBInsertActions(self.actions)
        
        #+++++++++GESTION DU LOG ICI
        #+++++++++ ANALYTICS
        
        print('routine flushed out')
        
    def getActions(self):
        return self.actionList

class Account:
    def __init__(self, username, password):
        self.username = username
        self.password = password
        
class FollowRoutine(Routine):   
    
    def __init__(self, account, username):
        super().__init__(account)

        self.username=username
        self.account = account
        
        self.routineDesc = 'Follow a person'
        self.routineName = 'Follow routine'
        
    def start(self):
        super().start()
        
        driver=webdriver.Chrome()
        
        actions.login(driver, self.account.username, self.account.password)
        
        actions.follow(self.actions, driver, self.username) #call an action
        
        super().stop()
        


class FollowFollowers(Routine):
    def __init__(self,account,action_limit,usernames):
        super().__init__()

        
        self.routineDesc = 'Follow the followers of a a person or list of persons, preferably popular'
        self.routineName = 'Follow Followers Routine'
        
        self.account = account
        self.usernames = usernames
        self.action_limit = action_limit #max number of action authorized whithin routine
        
    def start(self):
        try:
            super().start()
            
            driver=webdriver.Chrome()
            
            actions.login(driver, self.account.username, self.account.password)
            time.sleep(3)
            
            print('action limit:', self.action_limit)
            
            for username in self.usernames:
                action_limit_i = (self.action_limit+self.action_limit*(self.usernames.index(username)))/len(self.usernames) #number of action allocated for the following action function
                print(action_limit_i)
                actions.followFollowers(self.actions,driver,username,action_limit_i)
            
            super().stop()
        except WebDriverException:
            print("WebDriver Exception")
            super().stop()
        else:    
            super().stop()

        
class AnalyticsRoutine1(Routine):
    def __init__(self,driver,routine):
        super().__init__()
        self.username=username
        self.routineDesc = 'For a given routine, compare actions, and the results in terms of followers number.'
    
    def start(self):
        super().start()
        
        #actions.getFollowers(self.actions,self.driver,self.username)
        
        #get routine actions from DB
        #get new followers from DB
        
        super().stop()

class Cleaning(Routine):
    def __init__(self,account):
        super().__init__()

        
        self.routineDesc = "Clean person that didn't follow"
        self.routineName = 'Cleaning Routine'
        
        self.account = account
        
    def start(self):
        super().start()
        
        driver=webdriver.Chrome()
        
        actions.login(driver, self.account.username, self.account.password)
        time.sleep(3)
        

        
        #actions.updateFollowers2(self.actions,driver,self.account.username,DBGetFollowBack(), 9999)
        #actions.cleaning(self.actions,driver,self.account.username,DBGetUnfollow())
        super().stop()
        
class LikeFollowing(Routine):
    def __init__(self,account):
        super().__init__()
        
        self.account = account
        self.routineDesc = "Like 3 posts of each following"
        self.routineName = "Like Following Routine"
        self.maxActions = 9
        
    def start(self, driver = webdriver.Chrome()):
        super().start()
        
        actions.login(driver, self.account.username, self.account.password)
        
        time.sleep(2)
        
        usernames = actions.getFollowingUsernames(driver, self.account.username)
        
        for username in usernames :
            if(len(self.actionList) > self.maxActions):
                break
                
            driver.get("https://www.instagram.com/"+username+"/")
            time.sleep(1)
            
            posts = actions.getPosts(driver)

            likeCount = 0

            for p in posts:

                print(p)
                p.click()

                time.sleep(2)

                res = actions.likePost(driver)
                self.actionList.append(res)
                    
                if(res is not None):
                    likeCount+=1

                if(likeCount==3):
                    break


                driver.execute_script("window.history.go(-1)")

                time.sleep(0.3)
        
        
        super().stop()
        
        